@extends('layouts.dashboard')
@section('title', 'Thêm user mới')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="full_name">Họ tên</label>
                            <input type="text" class="form-control" value="{{ old('full_name') }}"
                                   id="full_name"
                                   name="full_name"
                                   placeholder="full name" required>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="phone">Số điện thoại</label>
                            <input type="text" class="form-control" value="{{ old('phone') }}"
                                   id="phone"
                                   name="phone"
                                   placeholder="phone" required>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="password">Mật khẩu</label>
                            <input type="password" class="form-control" name="password" value="" placeholder="input password" required>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="address">Địa chỉ</label>
                            <input type="text" class="form-control" value="{{ old('address') }}"
                                   id="address"
                                   name="address"
                                   placeholder="address">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <input type="text" class="form-control" value="{{ old('description') }}"
                                   id="description"
                                   name="description"
                                   placeholder="description">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sex">Giới tính</label>
                            <select name="sex" class="form-control" id="">
                                <option value="1">nữ</option>
                                <option value="2">nam</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1">hoạt động</option>
                                <option value="2">đã xác thực</option>
                                <option value="3">khoá</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="rating">Đánh giá</label>
                            <input type="number" class="form-control" name="rating" value="{{ old('rating', 0) }}" placeholder="rating">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="rating">Ngày sinh</label>
                            <input type="date" class="form-control" name="rating" value="{{ old('birthday') }}" placeholder="birthday">
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>
    <script type="text/javascript">
        $('#AppName').autocomplete({
            serviceUrl: '/application/search',
            onSelect: function (suggestion) {
                $('#EventAppID').val(suggestion.data)
            }
        });
    </script>
@stop