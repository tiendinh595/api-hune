@extends('layouts.dashboard')
@section('title', 'Quản lý thành viên')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="col-sm-12">
                    <form action="" method="get" class="form-inline text-center">
                        <input type="text" class="form-control" placeholder="Tên member" name="UserName" value="{{ app('request')->input('UserName') }}">
                        <input type="submit" class="btn btn-success" value="search">
                        <a href="/inside/user" class="btn btn-danger">reset</a>
                    </form>
                </div>
                <div class="clearfix"></div>
                <br>

                <table class="table m-0 table-colored table-inverse">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Họ tên</th>
                        <th>Sô điện thoại</th>
                        <th>Loạii ctài khoản</th>
                        <th>facebook id</th>
                        <th>Ảnh đại diện</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->phone}}</td>
                            <td>{{ $user->facebook_id == '' ? 'account' : 'facebook' }}</td>
                            <td>{{ $user->facebook_id }}</td>
                            <td><img src="{{ $user->avatar }}" width="60px" height="60px" alt=""></td>
                            <td>{!! $user->status == 1 ? '<span class="label label-success">active</span>' : ($user->status == 2 ? '<span class="label label-info">verified</span>' : '<span class="label label-danger">banned</span>')  !!}</td>
                            <td width="15%" class="text-right">
                                <a href="/inside/post/add?user_id={{$user->id}}&user={{$user->full_name}}"
                                   class="btn btn-sm btn-info"><i
                                            class="fa fa-plus"></i></a>
                                <a href="{{ route('member-edit', [$user->id]) }}"
                                   class="btn btn-sm btn-success"><i
                                            class="fa fa-edit"></i></a>
                                <a href="{{ route('member-delete', [$user->id]) }}"
                                   class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="text-center">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
@stop

@section('script')
    <script type="text/javascript">
        $('.btn-delete').click(function (e) {
            e.preventDefault();
            var url_delete = $(this).attr('href');
            var wraper = $(this).parents('tr');
            swal({
                        title: "Xóa member",
                        text: "Tất cả dữ liệu thuộc member này sẽ bị xóa không thể khôi phục",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        $.ajax({
                            'url': url_delete,
                            'success': function (data) {
                                if (data == 'ok') {
                                    swal("Deleted!", "Xóa member thành công", "success");
                                    wraper.remove();
                                } else {
                                    swal("Failed", "Xóa member thất bại", "error");
                                }
                            }
                        });
                    });
        });

    </script>
@stop