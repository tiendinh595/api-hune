@extends('layouts.dashboard')
@section('title', 'Cập nhật thông tin user')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="full_name">Họ tên</label>
                            <input type="text" class="form-control" value="{{ $user->full_name }}"
                                   id="full_name"
                                   name="full_name"
                                   placeholder="full name">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="facebook_id">Facebook ID</label>
                            <input type="text" class="form-control" value="{{ $user->facebook_id }}"
                                   id="facebook_id"
                                   name="facebook_id"
                                   placeholder="facebook id" disabled>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="phone">Số điện thoại</label>
                            <input type="text" class="form-control" value="{{ $user->phone }}"
                                   id="phone"
                                   name="phone"
                                   placeholder="phone">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="address">Địa chỉ</label>
                            <input type="text" class="form-control" value="{{ $user->address }}"
                                   id="address"
                                   name="address"
                                   placeholder="address">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <input type="text" class="form-control" value="{{ $user->description }}"
                                   id="description"
                                   name="description"
                                   placeholder="description">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sex">Giới tính</label>
                            <select name="sex" class="form-control" id="">
                                <option value="1" {{ $user->sex == 1 ? "selected='selected'" : '' }}>nữ</option>
                                <option value="2" {{ $user->sex == 2 ? "selected='selected'" : '' }}>nam</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" {{ $user->status == 1 ? 'selected="selected"' : '' }}>active</option>
                                <option value="2" {{ $user->status == 2 ? 'selected="selected"' : '' }}>verified</option>
                                <option value="0" {{ $user->status == 3 ? 'selected="selected"' : '' }}>banned</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="rating">Đánh giá</label>
                            <input type="text" class="form-control" name="rating" value="{{ $user->rating }}" placeholder="rating">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="password">Mật khẩu mới</label>
                            <input type="password" class="form-control" name="password" value="" placeholder="input new password">
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>
    <script type="text/javascript">
        $('#AppName').autocomplete({
            serviceUrl: '/application/search',
            onSelect: function (suggestion) {
                $('#EventAppID').val(suggestion.data)
            }
        });
    </script>
@stop