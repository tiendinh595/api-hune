@extends('layouts.dashboard')
@section('title', 'Add new permission')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Module</label>
                            <input type="text" class="form-control" value="{{ old('name') }}"
                                   id="name"
                                   name="name"
                                   placeholder="tên">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Chức năng</label>
                            <select name="performs[]" id="" multiple class="form-control" style="height: 177px">
                                <option value="list" selected>danh sách</option>
                                <option value="view" selected>xem chi tiết</option>
                                <option value="add" selected>thêm mới</option>
                                <option value="edit" selected>chỉnh sửa</option>
                                <option value="delete" selected>xoá</option>
                            </select>
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                    </p>
                </form>
            </div>
        </div>
        @stop

        @section('css')
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
            <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
        @stop

        @section('script')
            <script type="text/javascript">

                var elems = document.querySelectorAll('.js-switch');

                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { size: 'small' });
                }

                $('#AppDescription').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                  // set focus to editable area after initializing summernote
                });
                $('#Publisher').autocomplete({
                    serviceUrl: '/publisher/search',
                    onSelect: function (suggestion) {
                        $('#PublisherID').val(suggestion.data)
                    }
                });

            </script>
@stop