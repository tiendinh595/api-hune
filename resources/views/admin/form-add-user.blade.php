@extends('backend.layouts.dashboard')
@section('title', 'edit user')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('backend.includes.errors')
                @include('backend.includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type="text" class="form-control" value="{{ old('name') }}"
                                   id="name"
                                   name="name"
                                   placeholder="tên">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" value="{{ old('email') }}"
                                   id="email"
                                   name="email"
                                   placeholder="Email">
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Permissions</label>
                            <table class="table table-bordered">
                                <thead>
                                    <th></th>
                                    <th class="text-center" >List</th>
                                    <th class="text-center">View</th>
                                    <th class="text-center">Create</th>
                                    <th class="text-center">Update</th>
                                    <th class="text-center">Delete</th>
                                </thead>
                                <tbody>
                                    @foreach($permissions as $permission)
                                        <tr>
                                            <td width="20%"><b>{{$permission->name}}</b></td>
                                            <td style="padding:5px" class="text-center">
                                                <input type="checkbox" name="permission[{{$permission->name}}][read]" class="js-switch" >
                                            </td>
                                            <td style="padding:5px" class="text-center">
                                                <input type="checkbox" name="permission[{{$permission->name}}][view]" class="js-switch">
                                            </td>
                                            <td style="padding:5px" class="text-center">
                                                <input type="checkbox" name="permission[{{$permission->name}}][create]" class="js-switch" >
                                            </td>
                                            <td style="padding:5px" class="text-center">
                                                <input type="checkbox" name="permission[{{$permission->name}}][update]" class="js-switch">
                                            </td>
                                            <td style="padding:5px" class="text-center">
                                                <input type="checkbox" name="permission[{{$permission->name}}][delete]" class="js-switch">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                    </p>
                </form>
            </div>
        </div>
        @stop

        @section('css')
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
            <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
        @stop

        @section('script')
            <script type="text/javascript">

                var elems = document.querySelectorAll('.js-switch');

                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { size: 'small' });
                }

                $('#AppDescription').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                  // set focus to editable area after initializing summernote
                });
                $('#Publisher').autocomplete({
                    serviceUrl: '/publisher/search',
                    onSelect: function (suggestion) {
                        $('#PublisherID').val(suggestion.data)
                    }
                });

            </script>
@stop