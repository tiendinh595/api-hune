@extends('backend.layouts.dashboard')
@section('title', 'edit role')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('backend.includes.errors')
                @include('backend.includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type="text" class="form-control" value="{{ $role->name }}"
                                   id="name"
                                   name="name"
                                   placeholder="tên">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" class="form-control" value="{{ $role->slug }}"
                                   id="slug"
                                   name="slug"
                                   placeholder="slug">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control"
                                   id="description"
                                   name="description"
                                   placeholder="Description">{{ $role->description }}</textarea>
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
    <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
@stop

@section('script')
    <script type="text/javascript">

        var el = document.querySelector('.js-switch');
        var init = new Switchery(el, {size: 'small'});

        $('#AppDescription').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                  // set focus to editable area after initializing summernote
        });
        $('#Publisher').autocomplete({
            serviceUrl: '/publisher/search',
            onSelect: function (suggestion) {
                $('#PublisherID').val(suggestion.data)
            }
        });

    </script>
@stop