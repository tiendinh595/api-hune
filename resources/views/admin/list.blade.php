@extends('layouts.dashboard')
@section('title', 'Quản lý quản trị viên')

@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/alertify/css/alertify.min.css') }}">
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/alertify/css/themes/default.rtl.css') }}">
@stop

@section('script')
    <script type="text/javascript" src="{{ url('resources/assets/plugin/alertify/alertify.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('body').on('click', '.trash-user', function (e) {
                e.preventDefault();

                var parent = $(this).parents('tr');

                alertify.confirm("Xác nhận xóa quản trị viên này", function () {
                    email = parent.find('.em').text();
                    $.get('/inside/administrator/delete?email=' + email).success(function (data) {
                        if (data['status'] == 'success') {
                            alertify.success(data['msg']);
                            parent.remove();
                        } else {
                            alertify.error(data['msg']);
                        }
                    });
                }, function () {

                });
            });

            $('.btn-add-new').click(function (e) {
                alertify.prompt('Thêm quản trị viên mới', 'Email', '@'
                        , function (evt, email) {
                            $.get('/inside/administrator/add?email=' + email).success(function (data) {
                                if (data['status'] == 'success') {
                                    alertify.success(data['msg']);
                                    $('tbody').append(' <tr><td></td><td></td><td class="em">' + email + '</td><td></td><td><a href="" class="trash-user"><i class="fa fa-times"></i></a></td> </tr>')
                                } else {
                                    alertify.error(data['msg']);
                                }
                            });
                        }
                        , function () {
                        });
            })
        })

    </script>

@stop

@section('content')
    <div class="col-md-12">
        <div class="card-box">
            <p class="text-right"><button class="btn btn-danger btn-add-new">Thêm mới</button> </p>

            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Avatar</th>
                    <th>Email</th>
                    <th>Tên</th>
                    <th style="width: 100px"></th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td><img src="{{ $user->avatar }}" alt=""></td>
                        <td class="em">{{ $user->username }}</td>
                        <td>{{ $user->name }}</td>
                        <td>
                            <a href="{{ route('edit-admin', [$user->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-pencil-square"></i></a>
                            <a href="" class="trash-user btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
