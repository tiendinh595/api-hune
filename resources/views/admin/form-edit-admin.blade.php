@extends('layouts.dashboard')
@section('title', 'Add new admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Họ tên</label>
                            <input type="text" class="form-control" value="{{ $user->name }}"
                                   id="name"
                                   name="name"
                                   placeholder="tên">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" value="{{ $user->email }}"
                                   id="email"
                                   name="email"
                                   placeholder="email">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="email">Phân quyền <a href="" class="label label-info" id="select_all">chọn tất
                                    cả</a></label>
                            <div class="clearfix"></div>
                            @foreach($permissions as $permission_name => $permission)
                                <div class="col-sm-6">
                                    <div><b>{{ $permission_name }}</b></div>
                                    <select name="permissions[]" id="" multiple class="form-control"
                                            style="height: 177px; margin-bottom: 10px">
                                        @foreach($permission as $p)
                                            <option value="{{ $p->name }}" {!! in_array($p->name, $user_has_permissions) ? 'selected="selected"' : '' !!}>{{ $p->description }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            @endforeach
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                    </p>
                </form>
            </div>
        </div>
        @stop

        @section('css')
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
            <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
        @stop

        @section('script')
            <script type="text/javascript">

                var is_selected_all = false;

                $('#select_all').click(function (e) {
                    e.preventDefault();
                    is_selected_all = !is_selected_all;

                    $('option').prop('selected', is_selected_all);
                });

                var elems = document.querySelectorAll('.js-switch');

                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], {size: 'small'});
                }

                $('#AppDescription').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                  // set focus to editable area after initializing summernote
                });
                $('#Publisher').autocomplete({
                    serviceUrl: '/publisher/search',
                    onSelect: function (suggestion) {
                        $('#PublisherID').val(suggestion.data)
                    }
                });

            </script>
@stop