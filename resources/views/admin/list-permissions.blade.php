@extends('layouts.dashboard')
@section('title', 'Phân quyền')

@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/alertify/css/alertify.min.css') }}">
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/alertify/css/themes/default.rtl.css') }}">
@stop

@section('script')
    <script type="text/javascript" src="{{ url('resources/assets/plugin/alertify/alertify.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('body').on('click', '.trash-user', function (e) {
                e.preventDefault();

                var parent = $(this).parents('tr');

                alertify.confirm("Xác nhận xóa quản trị viên này", function () {
                    email = parent.find('.em').text();
                    $.get('/administrator/delete?email=' + email).success(function (data) {
                        if (data['status'] == 'success') {
                            alertify.success(data['msg']);
                            parent.remove();
                        } else {
                            alertify.error(data['msg']);
                        }
                    });
                }, function () {

                });
            });


        })

    </script>

@stop

@section('content')
    <div class="col-md-12">
        <div class="card-box">
            <p class="text-right"><a href="{{ route('add-permission') }}" class="btn btn-danger btn-add-new">Thêm
                    mới</a></p>

            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th style="width: 60px"></th>
                </tr>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $permission->name }}</td>

                        <td>{{ $permission->description }}</td>
                        <td>
                            <a href="{{ route('edit-permission', [$permission->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
