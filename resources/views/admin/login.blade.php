
<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/adminox/default/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 04:06:50 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Adminox - Responsive Web App Kit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App css -->
    <link href="{{ url('resources/assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ url('resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>


<body class="bg-accpunt-pages">

<!-- HOME -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">

                    <div class="account-pages">
                        <div class="account-box">
                            <div class="account-logo-box">
                                <h2 class="text-uppercase text-center">
                                    <p>Sign In</p>
                                </h2>

                            </div>
                            <div class="account-content">
                                <form class="form-horizontal" action="#">
                                    <div class="form-group text-center m-t-10">
                                        <div class="col-xs-12">
                                            <a href="/inside/login-gg" class="btn btn-md btn-block btn-googleplus waves-effect waves-light" type="submit"><i class="fa fa-google">oogle</i></a>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- end card-box-->


                </div>
                <!-- end wrapper -->

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="../plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

</body>

<!-- Mirrored from coderthemes.com/adminox/default/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 04:06:50 GMT -->
</html>