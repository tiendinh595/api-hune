@extends('layouts.dashboard')
@section('title', 'edit permission')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type="text" class="form-control" value="{{ $permission->name }}"
                                   id="name"
                                   name="name"
                                   placeholder="tên" readonly>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control"
                                      id="description"
                                      name="description"
                                      placeholder="Description">{{ $permission->description }}</textarea>
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                    </p>
                </form>
            </div>
        </div>
        @stop

        @section('css')
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
            <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
        @stop

        @section('script')
            <script type="text/javascript">

                var elems = document.querySelectorAll('.js-switch');

                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], { size: 'small' });
                }

                $('#AppDescription').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                  // set focus to editable area after initializing summernote
                });
                $('#Publisher').autocomplete({
                    serviceUrl: '/publisher/search',
                    onSelect: function (suggestion) {
                        $('#PublisherID').val(suggestion.data)
                    }
                });

            </script>
@stop