@extends('layouts.dashboard')
@section('title', 'Cập nhật thông tin')
@section('content')
    <div class="col-md-12">
        <div class="card-box">
            <h3 class="box-title">Cập nhật thông tin</h3>
            @include('includes.errors')

            @if(Session::has('alert'))
                <div class="alert alert-{{ Session::get('alert')['class'] }}">{{ Session::get('alert')['msg'] }}</div>
            @endif

            <form method="post" action="">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Tên đăng nhập</label>
                    <input type="text" class="form-control" name="username" id="exampleInputEmail1"
                           placeholder="Tên đăng nhập"
                           value="{{ auth()->guard('admin')->user()->username }}" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tên</label>
                    <input type="text" class="form-control" name="name" id="exampleInputEmail1"
                           placeholder="Họ & tên"
                           value="{{ auth()->guard('admin')->user()->name }}">
                </div>
                <button type="submit" class="btn btn-primary">Cập nhật</button>
            </form>
        </div>
    </div>

@stop
