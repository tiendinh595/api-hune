
<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/adminox/default/page-404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 04:06:50 GMT -->
<head>
    <meta charset="utf-8" />
    <title>404 - HuneGroup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <!-- App css -->
    <link href="{{ url('resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('resources/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>


<body class="bg-accpunt-pages">

<!-- HOME -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">

                <div class="wrapper-page">
                    <div class="account-pages">
                        <div class="account-box">

                            {{--<div class="account-logo-box">--}}
                                {{--<h2 class="text-uppercase text-center">--}}
                                    {{--<a href="index.html" class="text-success">--}}
                                        {{--<span><img src="assets/images/logo_dark.png" alt="" height="30"></span>--}}
                                    {{--</a>--}}
                                {{--</h2>--}}
                            {{--</div>--}}

                            <div class="account-content">
                                <h1 class="text-error">403</h1>
                                <h2 class="text-uppercase text-danger m-t-30">Bạn không có quyền vào trang này</h2>

                                <a class="btn btn-md btn-block btn-primary waves-effect waves-light m-t-20" href="/inside"> Return Home</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->




<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ url('resources/assets/js/jquery.min.js') }}"></script>
<script src="{{ url('resources/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('resources/assets/js/metisMenu.min.js') }}"></script>
<script src="{{ url('resources/assets/js/waves.js') }}"></script>
<script src="{{ url('resources/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ url('resources/assets/plugin/sweet-alert2/sweetalert.min.js') }}"></script>

<!-- App js -->
<script src="{{ url('resources/assets/js/jquery.core.js') }}"></script>
<script src="{{ url('resources/assets/js/jquery.app.js') }}"></script>

</body>

<!-- Mirrored from coderthemes.com/adminox/default/page-404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 04:06:50 GMT -->
</html>