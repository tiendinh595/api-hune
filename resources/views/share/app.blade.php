<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HUNE - Việc làm thêm cho giới trẻ</title>

    <meta property="al:web:should_fallback" content="false" />
    <meta property="og:url"
          content="{{ url('share/app') }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="HUNE - Việc làm thêm cho giới trẻ"/>
    <meta property="og:description" content="Hune, kênh thông tin “việc làm” (viec lam) và “tuyển dụng” ( tuyen dung) dành cho giới trẻ. Nhanh chóng và đơn giản, tại bất cứ đâu, bất cứ lúc nào, với đa dạng nghành nghề công việc"/>
    <meta property="og:image"
          content="{{ url('/storage/upload/images/screenshoot.png') }}"/>

    <meta property="fb:app_id" content="581076048949837" />

    <meta property="al:ios:url" content="fb581076048949837://" />
    <meta property="al:ios:app_store_id" content="1227361450" />
    <meta property="al:ios:app_name" content="Hune - Kenh Thong Tin Viec Lam Va Tuyen Dung" />
    <meta property="al:android:url" content="com.hunegroup.hune://" />
    <meta property="al:android:app_name" content="HUNE - Việc làm thêm cho giới trẻ" />
    <meta property="al:android:package" content="com.hunegroup.hune" />
</head>
<body>
    {{--<script type="text/javascript">--}}
        {{--setTimeout(function () {--}}
            {{--window.location.href = "https://hunegroup.com";--}}
        {{--}, 500);--}}
    {{--</script>--}}
</body>
</html>