@extends('layouts.dashboard')
@section('title', 'Quản lý bình luận')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <table class="table m-0 table-colored table-inverse">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>thành viên</th>
                        <th>đối tượng</th>
                        <th>loại</th>
                        <th>đánh giá</th>
                        <th>nội dung</th>
                        <th>trạng thái</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($votes as $vote)
                        <tr>
                            <th scope="row">{{ $vote->id }}</th>
                            <td>{{ $vote->owner ? $vote->owner->full_name : '' }}</td>
                            <td>{!! $vote->type == 1 ? '<a href="/inside/user/'.($vote->user ? $vote->user->id :'').'/edit">'.($vote->user ? $vote->user->full_name : '').'</a>' : '<a href="/inside/post/'.($vote->post ? $vote->post->id : '').'/edit">'.($vote->post ? $vote->post->title : '').'</a>'  !!} </td>
                            <td>{{ $vote->type==1?'thành viên' : 'post' }}</td>
                            <td>{{ $vote->rate }}</td>
                            <td>{{ $vote->comment }}</td>
                            <td>
                                <input type="checkbox" class="js-switch i_status"
                                       data-id="{{ $vote->id }}" {{ $vote->status == 1 ? 'checked="checked"' : '' }} />
                            <td width="15%" class="text-right">
                                <a href="{{ route('comment-delete', [$vote->id]) }}"
                                   class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="text-center">
                    {{ $votes->links() }}
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
@stop

@section('script')
    <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
    <script type="text/javascript">

        var elems = document.querySelectorAll('.js-switch');

        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i]);
        }

        $('input[type=checkbox]').change(function (e) {
            e.preventDefault();

            var id = $(this).attr('data-id');
            $.get('/inside/comment/' + id + '/change-status').success(function (res) {
            })
        });

        $('.btn-delete').click(function (e) {
            e.preventDefault();
            var url_delete = $(this).attr('href');
            var wraper = $(this).parents('tr');
            swal({
                    title: "Xóa comment",
                    text: "Tất cả dữ liệu thuộc comment này sẽ bị xóa không thể khôi phục",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        'url': url_delete,
                        'success': function (data) {
                            if (data == 'ok') {
                                swal("Deleted!", "Xóa comment thành công", "success");
                                wraper.remove();
                            } else {
                                swal("Failed", "Xóa comment thất bại", "error");
                            }
                        }
                    });
                });
        });

    </script>
@stop