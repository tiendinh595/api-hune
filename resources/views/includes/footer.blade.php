<script src="{{ url('resources/assets/js/jquery.js') }}"></script>
<script src="{{ url('resources/assets/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ url('resources/assets/js/jquery.slimscroll.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('resources/assets/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
{{--<script src="{{ url('resources/assets/js/alertify.js') }}"></script>--}}

<script type="text/javascript">
    $(document).ready(function () {
        var href = document.location.href;
        list_segment = href.split('/');
        console.log(list_segment);
        var path = '';
        for (var i = 3; i < list_segment.length; i++) {
            path += '/'+list_segment[i];
        }

        var current_node = $('a[href="'+path+'"]');
        current_node.parents('li').addClass('active');
        current_node.parents('ul').addClass('menu-open');
        current_node.css("color", "green")
    });
</script>

@yield('script')