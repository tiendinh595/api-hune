<meta charset="utf-8" />
<title>@yield('title') - Hune</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="Coderthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- App css -->
<link href="{{ url('resources/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/assets/plugin/sweet-alert2/sweetalert.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ url('resources/assets/js/jquery.min.js') }}"></script>

<script src="{{ url('resources/assets/js/modernizr.min.js') }}"></script>

@yield('css')