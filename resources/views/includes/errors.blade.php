@if($errors->all())
    <div class="alert alert-danger alert-white alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        @foreach($errors->all() as $error)
            {{ $error }} <br>
        @endforeach
    </div>
@endif