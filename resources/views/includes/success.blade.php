@if(Session::has('msg'))
    <div class="alert alert-success alert-white alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ session('msg') }}
    </div>
@endif