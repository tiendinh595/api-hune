<?php

$arr_menus = [
    'dahboard' => [
        'info' => ['title' => 'Dashboard', 'href' => '/inside', 'class' => 'fi-air-play', 'permission' => '']
    ],
    'notification' => [
        'info' => ['title' => 'Notification', 'href' => '', 'class' => 'fa fa-bell', 'permission' => ''],
        'child' => [
            ['title' => 'Tất cả', 'href' => '/inside/notification', 'permission' => 'list-notification'],
            ['title' => 'Bắn thông báo', 'href' => '/inside/notification/push', 'permission' => 'add-notification'],
        ]
    ],
    'user' => [
        'info' => ['title' => 'Quản lý user', 'href' => '', 'class' => 'fa fa-users', 'permission' => ''],
        'child' => [
            ['title' => 'Tất cả', 'href' => '/inside/user', 'permission' => 'list-user'],
            ['title' => 'Thêm mới', 'href' => '/inside/user/add', 'permission' => 'add-user'],
        ]
    ],
    'administrator' => [
        'info' => ['title' => 'Quản trị', 'href' => '', 'class' => 'fi-head', 'permission' => ''],
        'child' => [
            ['title' => 'Tất cả', 'href' => '/inside/administrator', 'permission' => 'list-admin'],
            ['title' => 'Thêm mới', 'href' => '/inside/administrator/add', 'permission' => 'add-admin'],
            ['title' => 'Phân quyền', 'href' => '/inside/administrator/permission', 'permission' => 'list-permission'],
        ]
    ],
    'sigout' => [
        'info' => ['title' => 'Thoát', 'href' => '/logout', 'class' => 'fi-power', 'permission' => '']
    ]
];

?>

<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metisMenu nav" id="side-menu">
                <li class="menu-title">Navigation</li>
                @foreach($arr_menus as $key => $menu)
                    @if(isset($menu['child']))
                        @if($menu['info']['permission']!='' && !auth()->guard('admin')->user()->can($menu['info']['permission']))
                            <?php continue ?>
                        @endif
                        <li class="{{ $key }}">
                            <a href="javascript: void(0);" aria-expanded="true"><i
                                        class="{{ $menu['info']['class'] }}"></i>
                                <span> {{ $menu['info']['title'] }} </span> <span class="menu-arrow"></span></a>
                            <ul class="nav-second-level nav" aria-expanded="true">
                                @foreach($menu['child'] as $child)
                                    @if($child['permission']=='')
                                        <li><a href="{{ $child['href'] }}"> {{ $child['title'] }}</a></li>
                                    @elseif(auth()->guard('admin')->user()->can($child['permission']))
                                        <li><a href="{{ $child['href'] }}"> {{ $child['title'] }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @else
                        @if($menu['info']['permission']=='')
                            <li><a href="{{ $menu['info']['href'] }}"><i class="{{ $menu['info']['class'] }}"></i>
                                    <span> {{ $menu['info']['title'] }} </span></a></li>
                        @elseif(auth()->guard('admin')->user()->can($menu['info']['permission']))
                            <li><a href="{{ $menu['info']['href'] }}"><i class="{{ $menu['info']['class'] }}"></i>
                                    <span> {{ $menu['info']['title'] }} {{ $menu['info']['permission'] }} </span></a>
                            </li>
                        @endif

                    @endif
                @endforeach
            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>