@extends('layouts.dashboard')
@section('title', 'Đăng ')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <input type="hidden" name="latitude" id="latitude">
                    <input type="hidden" name="longitude" id="longitude">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="type">Loại bài đăng</label>
                            <select name="type" id="type" class="form-control">
                                <option value="1" {{ old('type') == 1 ? 'selected="selected"' : '' }}>Tuyển dụng
                                </option>
                                <option value="2" {{ old('type') == 2 ? 'selected="selected"' : '' }}>Tìm việc</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="type">Thành viên</label>
                            <input type="hidden" name="user_id" value="{{ old('user_id', app('request')->get('user_id')) }}" id="user_id">
                            <input type="text" id="user" class="form-control" name="user" value="{{ old('user', app('request')->get('user')) }}">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="title">Tiêu đề</label>
                            <input type="text" class="form-control" value="{{ old('title') }}"
                                   id=""
                                   name="title"
                                   placeholder="">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control"
                                      id="description"
                                      name="description"
                                      placeholder="">{{ old('description') }}</textarea>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="category_parent_id">Chuyên mục cha</label>
                            <select name="category_parent_id" id="category_parent_id" class="form-control">
                                @foreach($parent_categories as $parent_category)
                                    <option value="{{ $parent_category->id }}" {{ old('category_parent_id') == $parent_category->id ? "selected='selected'" : '' }}>{{ $parent_category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="category_id">Chuyên mục con</label>
                            <select name="category_id" id="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? "selected='selected'" : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="star">Số sao</label>
                            <input type="text" class="form-control" value="{{ old('rating') }}"
                                   id="star"
                                   name="rating"
                                   placeholder="star">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="quantity">Số lượng</label>
                            <input type="text" class="form-control" value="{{ old('quantity') }}"
                                   id="quantity"
                                   name="quantity"
                                   placeholder="quantity">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sex">Địa chỉ</label>
                            <input type="text" name="address" value="{{ old('address') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Lương</label>
                            <input type="text" name="salary" value="{{ old('salary') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary_type">Loại Lương</label>
                            <select name="salary_type" class="form-control">
                                <option value="1" {!! old('salary_type') == 1 ? 'selected="selected"' : ''!!}>theo giờ
                                </option>
                                <option value="2" {!! old('salary_type') == 2 ? 'selected="selected"' : ''!!}>theo
                                    ngày
                                </option>
                                <option value="3" {!! old('salary_type') == 3 ? 'selected="selected"' : ''!!}>theo
                                    tuần
                                </option>
                                <option value="4" {!! old('salary_type') == 4 ? 'selected="selected"' : ''!!}>theo
                                    tháng
                                </option>
                                <option value="5" {!! old('salary_type') == 5 ? 'selected="selected"' : ''!!}>theo năm
                                </option>
                                <option value="5" {!! old('salary_type') ==  6? 'selected="selected"' : ''!!}>thoả thuận
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Hình chính</label>
                            <input type="file" name="thumbnail" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Hình mô tả 1</label>
                            <input type="file" name="image1" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Hình mô tả 2</label>
                            <input type="file" name="image2" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Hình mô tả 3</label>
                            <input type="file" name="image3" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="start_date">Ngày bắt đầu</label>
                            <input type="date" name="start_date" value="{{ old('start_date') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="end_date">Ngày kết thúc</label>
                            <input type="date" name="end_date" value="{{ old('end_date') }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" {{ old('status') == 1 ? 'selected="selected"' : '' }}>on</option>
                                <option value="2" {{ old('status') == 2 ? 'selected="selected"' : '' }}>off</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="end_date">Vị trí</label>
                            <a href="#md_map"  data-toggle="modal" data-id="Peggy Guggenheim Collection - Venice">chọn vị trí</a>

                        </div>
                    </div>

                    <!-- Modal -->
                    <div id="md_map" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                    <input type="text" class="form-control" id="us3-address" placeholder="Nhập vị trí">
                                    <div id="map"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--<div id="somecomponent" style="width: 500px; height: 400px;"></div>--}}

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>
                    </p>
                </form>
            </div>
        </div>
    </div>


@stop

@section('css')
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 400px;
            width:500px;
        }

        /* Optional: Makes the sample page fill the window. */

        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
        .pac-container {
            z-index: 100000;
        }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyArlPfkDafT7PqG9PzA69GlfptcBSvJ3wQ&libraries=places"></script>

    <script src="{{ url('resources/assets/js/locationpicker.jquery.min.js') }}"></script>
@stop

@section('script')
    <script src="{{ url('resources/assets/js/locationpicker.jquery.min.js') }}"></script>

    <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('select[name="type"]').change(function (e) {
                value = $(this).val();

                if (value == '1') {
                    setVisibleForControll(false);
                } else {
                    setVisibleForControll(true);
                }
            });

            function setVisibleForControll($value) {
                $('#quantity').prop('disabled', $value);
                $('input[name="salary"]').prop('disabled', $value);
                $('input[name="start_date"]').prop('disabled', $value);
                $('input[name="end_date"]').prop('disabled', $value);
                $('select[name="salary_type"]').prop('disabled', $value);
            }


            $('#user').autocomplete({
                serviceUrl: '/inside/user/search',
                onSelect: function (suggestion) {
                    $('#user_id').val(suggestion.data)
                }
            });


            $('#category_parent_id').change(function (e) {
                $id = $(this).val();
                $.get('/inside/category/list/' + $id).success(function (data) {
                    $('#category_id').html(data)
                })
            });

            $('#myModal').on('shown', function () {
                google.maps.event.trigger(map, "resize");
            });

        });

        var is_ready_location = false;

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                alert('Không thê lấy được vị trí hiện tại')
            }
        }

        var latitude, longitude;

        function showPosition(position) {
            is_ready_location = true;
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
        }

        function showError(error) {
            $('#md_map').modal('hide');
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    alert("User denied the request for Geolocation.")
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("Location information is unavailable.")
                    break;
                case error.TIMEOUT:
                    alert("The request to get user location timed out.")
                    break;
                case error.UNKNOWN_ERROR:
                    alert("An unknown error occurred.")
                    break;
            }
        }

        getLocation();

        $('#md_map').on('shown.bs.modal', function () {

            if(is_ready_location) {
                var map = $('#map').locationpicker({
                    enableAutocomplete: true,
                    enableReverseGeocode: true,
                    location: {
                        latitude: latitude,
                        longitude: longitude
                    },
                    radius: 300,
                    inputBinding: {
                        latitudeInput: $('#latitude'),
                        longitudeInput: $('#longitude'),
                        locationNameInput: $('#us3-address')
                    },
                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                        var addressComponents = $(this).locationpicker('map').location.addressComponents;
                        console.log(currentLocation);  //latlon
                        console.log($('#latitude').val());
                        console.log($('#longitude').val());
                        updateControls(addressComponents); //Data
                    }

                });


                function updateControls(addressComponents) {
//                console.log(addressComponents);
                }
            } else {
                $('#md_map').modal('hide');

                alert('đang xác định vị trí ....');
            }


        });


    </script>
@stop