@extends('layouts.dashboard')
@section('title', 'Quản lý post')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="col-sm-12">
                    <form action="" method="get" class="form-inline text-center">
                        <input type="hidden" name="user_id" id="user_id" value="{{ app('request')->input('user_id') }}">
                        <select name="type" id="" class="form-control">
                            <option value="">all</option>
                            <option value="1" {{ app('request')->get('type') == 1 ? "selected='selected'" : ''}}>Tuyen dung</option>
                            <option value="2" {{ app('request')->get('type') == 2 ? "selected='selected'" : ''}}>Tim viec</option>
                        </select>
                        <input type="text" class="form-control" placeholder="user" size="30" name="user" id="user" value="{{ app('request')->input('user') }}">
                        <input type="submit" class="btn btn-success" value="search">
                        <a href="/inside/post" class="btn btn-danger">reset</a>
                    </form>
                </div>
                <div class="clearfix"></div>
                <br>

                <table class="table m-0 table-colored table-inverse">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tiêud đề</th>
                        <th>Chuyên mục</th>
                        <th>Chuyên mục con</th>
                        <th>Người đăng</th>
                        <th>Loại</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <th scope="row">{{ $post->id }}</th>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->parent_category != null ? $post->parent_category->name : '' }}</td>
                            <td>{{ $post->category != null ? $post->category->name : '' }}</td>
                            <td><a href="/inside/post?user_id={{ $post->user ? $post->user->id : '' }}&user={{ $post->user ? $post->user->full_name : '' }}">{{ $post->user ? $post->user->full_name : '' }}</a></td>
                            <td>{!! $post->type == 1 ? '<span class="label label-info">tuyen dung</span>' : '<span class="label label-success">tim viec</span>' !!}</td>
                            <td>{!! $post->status == 1 ? '<span class="label label-success">on</span>' : '<span class="label label-success">off</span>' !!}</td>
                            <td width="15%" class="text-right">
                                <a href="{{ route('post-edit', [$post->id]) }}"
                                   class="btn btn-sm btn-success"><i
                                            class="fa fa-edit"></i></a>
                                <a href="{{ route('post-delete', [$post->id]) }}"
                                   class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="text-center">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
@stop

@section('script')

    <script src="{{ url('resources/assets/plugin/summernote/summernote.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>

    <script type="text/javascript">
        $('.btn-delete').click(function (e) {
            e.preventDefault();
            var url_delete = $(this).attr('href');
            var wraper = $(this).parents('tr');
            swal({
                title: "Xóa post",
                text: "Tất cả dữ liệu thuộc post này sẽ bị xóa không thể khôi phục",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            },
            function () {
                $.ajax({
                    'url': url_delete,
                    'success': function (data) {
                        if (data == 'ok') {
                            swal("Deleted!", "Xóa post thành công", "success");
                            wraper.remove();
                        } else {
                            swal("Failed", "Xóa post thất bại", "error");
                        }
                    }
                });
            });
        });

        $('#user').autocomplete({
            serviceUrl: '/inside/user/search',
            onSelect: function (suggestion) {
                $('#user_id').val(suggestion.data)
            }
        });

    </script>
@stop



@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/summernote/summernote.css') }}">
    <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
@stop
