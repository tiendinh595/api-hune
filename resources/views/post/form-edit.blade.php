@extends('layouts.dashboard')
@section('title', 'Cập nhật thông tin post')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="title">Tiêu đề</label>
                            <input type="text" class="form-control" value="{{ $post->title }}"
                                   id="title"
                                   name="title"
                                   placeholder="full name">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="description">Mô tả</label>
                            <textarea class="form-control"
                                      id="description"
                                      name="description"
                                      placeholder="">{{ $post->description }}</textarea>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="category_parent_id">Chuyên mục cha</label>
                            <select name="category_parent_id" id="category_parent_id" class="form-control">
                                @foreach($parent_categories as $parent_category)
                                    <option value="{{ $parent_category->id }}" {{ $post->category_parent_id == $parent_category->id ? "selected='selected'" : '' }}>{{ $parent_category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="category_id">Chuyên mục con</label>
                            <select name="category_id" id="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ $post->category_id == $category->id ? "selected='selected'" : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="star">Số sao</label>
                            <input type="text" class="form-control" value="{{ $post->rating }}"
                                   id="star"
                                   name="rating"
                                   placeholder="star">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="quantity">Số lượng</label>
                            <input type="text" class="form-control" value="{{ $post->quantity }}"
                                   id="quantity"
                                   name="quantity"
                                   placeholder="quantity">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="sex">Địa chỉ</label>
                            <input type="text" name="address" value="{{ $post->address }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="longitude">Bản đồ</label>
                            <a class="googleMapPopUp" rel="nofollow"
                               href="https://maps.google.com/?q={{ $post->latitude }},{{ $post->longitude }}"
                               target="_blank">View location
                                map </a>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="salary">Lương</label>
                            <input type="text" name="salary" value="{{ $post->salary }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="start_date">Ngày bắt đầu</label>
                            <input type="date" name="start_date" value="{{ $post->start_date }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="end_date">Ngày kết thúc</label>
                            <input type="date" name="saend_datelary" value="{{ $post->end_date }}" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" {{ $post->status == 1 ? 'selected="selected"' : '' }}>on</option>
                                <option value="2" {{ $post->status == 2 ? 'selected="selected"' : '' }}>off</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Hình chính <a href="{{ $post->thumbnail }}"
                                                              target="_blank">{{ $post->thumbnail }}</a></label>
                            <input type="file" name="thumbnail" class="form-control" accept="image/*">

                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Hình mô tả
                                1 {!! isset($post->images[0]) ? '<a href="'.$post->images[0].'"  target="_blank">'.$post->images[0].'</a>' : '' !!}</label>
                            <input type="file" name="image1" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Hình mô tả
                                2 {!! isset($post->images[1]) ? '<a href="'.$post->images[1].'"  target="_blank">'.$post->images[1].'</a>' : '' !!}</label>
                            <input type="file" name="image2" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="status">Hình mô tả
                                3 {!! isset($post->images[2]) ? '<a href="'.$post->images[2].'"  target="_blank">'.$post->images[2].'</a>' : '' !!}</label>
                            <input type="file" name="image3" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/colorbox/colorbox.css') }}">
@stop

@section('script')
    <script src="{{ url('resources/assets/plugin/colorbox/jquery.colorbox.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#category_parent_id').change(function (e) {
                $id = $(this).val();
                $.get('/inside/category/list/' + $id).success(function (data) {
                    $('#category_id').html(data)
                })
            })

            $('.googleMapPopUp').each(function () {
                var thisPopup = $(this);
                thisPopup.colorbox({
                    iframe: true,
                    innerWidth: 600,
                    innerHeight: 500,
                    opacity: 0.7,
                    href: thisPopup.attr('href') + '&ie=UTF8&t=h&output=embed'
                });
            });
        })
    </script>
@stop