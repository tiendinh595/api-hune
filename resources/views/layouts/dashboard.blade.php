<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/adminox/default/page-starter.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Jun 2017 04:06:50 GMT -->
<head>
    @include('includes.head')
</head>


<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
@include('includes.header')
<!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
@include('includes.left')
<!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">@yield('title')</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                @yield('content')

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            2017 © Inside HUNE
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- jQuery  -->
<script src="{{ url('resources/assets/js/jquery.min.js') }}"></script>
<script src="{{ url('resources/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('resources/assets/js/metisMenu.min.js') }}"></script>
{{--<script src="{{ url('resources/assets/js/waves.js') }}"></script>--}}
<script src="{{ url('resources/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ url('resources/assets/plugin/sweet-alert2/sweetalert.min.js') }}"></script>

<!-- App js -->
<script src="{{ url('resources/assets/js/jquery.core.js') }}"></script>
<script src="{{ url('resources/assets/js/jquery.app.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var href = document.location.href;
        list_segment = href.split('/');
        $('.'+list_segment[3]).addClass('active').css('color', 'green');
        $('.'+list_segment[3]).find('ul').addClass('collapse').addClass('in');
    });
</script>

@yield('script')

</body>
</html>