@extends('layouts.dashboard')
@section('title', 'Push notification')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="title">Tiêu đề</label>
                            <input type="text" class="form-control" value="{{ old('title') }}"
                                   id="title"
                                   name="title"
                                   placeholder="title">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="content">Tin nhắn</label>
                            <textarea name="content" id="" class="form-control">{{ old('content') }}</textarea>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="phone">Loại</label>
                            <select name="type" class="form-control" id="">
                                <option value="1" {{ old('type') == 1 ? "selected='selected'" : '' }}>Tuyển dụng
                                </option>
                                <option value="2" {{ old('type') == 2 ? "selected='selected'" : '' }}>Tìm việc
                                </option>
                                <option value="3" {{ old('type') == 3 ? "selected='selected'" : '' }}>Hệ thống
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="phone">Kênh</label>
                            <select name="chanel" id="chanel" class="form-control">
                                <option value="1" {{ old('type') == 1 ? "selected='selected'" : '' }}>chung
                                </option>
                                <option value="2" {{ old('type') == 2 ? "selected='selected'" : '' }}>riêng
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="phone">Người nhận(chỉ nhập khi chọn kênh riêng)</label>
                            <input type="hidden" name="user_id" id="user_id">
                            <input type="text" name="user" id="user" class="form-control" placeholder="user" disabled>
                        </div>
                    </div>
                    <br>
                    <br>

                    <p class="text-center">
                        <button type="submit" class="btn btn-info waves-effect waves-light" style="margin-top: 10px">
                            Gủi
                        </button>
                    </p>

                </form>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>
    <script type="text/javascript">

        $('#chanel').change(function (e) {
            if ($(this).val() == '1') {
                $('#user').prop('disabled', true);
            } else {
                $('#user').prop('disabled', false);
            }
        });

        $('#user').autocomplete({
            serviceUrl: '/inside/user/search',
            onSelect: function (suggestion) {
                $('#user_id').val(suggestion.data)
            }
        });

    </script>
@stop



@section('css')
    <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
@stop
