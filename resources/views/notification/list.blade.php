@extends('layouts.dashboard')
@section('title', 'Quản lý notification')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <button class="btn btn-danger" id="delete_all">Xoá đã chọn</button>
                <br><br>
                <table class="table m-0 table-colored table-inverse">
                    <thead>
                    <tr>
                        <th width="15px"><input type="checkbox" id="select_all"></th>
                        <th>#</th>
                        <th>Tiêu đề</th>
                        <th>Tin nhắn</th>
                        <th>Loại</th>
                        <th>Kênh</th>
                        <th>Ngày gửi</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notifications as $notification)
                        <tr>
                            <td><input type="checkbox" class="i_s"
                                       value="{{ route('notification-delete', [$notification->id]) }}"></td>
                            <th scope="row">{{ $notification->id }}</th>
                            <td>{{ $notification->title }}</td>
                            <td>{{ $notification->content }}</td>
                            <td>{{ $notification->type == 1 ? 'tuyển dụng' : ($notification->type==2? 'tìm việc' : 'hệ thống') }}</td>
                            <td>{{ $notification->chanel == 1 ? 'chung' : 'riêng' }}</td>
                            <td>{{ $notification->created_at }}</td>
                            <td width="15%" class="text-right">
                                <a href="{{ route('notification-edit', [$notification->id]) }}"
                                   class="btn btn-sm btn-success"><i
                                            class="fa fa-edit"></i></a>
                                <a href="{{ route('notification-delete', [$notification->id]) }}"
                                   class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="text-center">
                    {{ $notifications->links() }}
                </div>
            </div>
        </div>
    </div>
@stop


@section('css')
@stop

@section('script')
    <script type="text/javascript">
        $('.btn-delete').click(function (e) {
            e.preventDefault();
            var url_delete = $(this).attr('href');
            var wraper = $(this).parents('tr');
            swal({
                    title: "Delete notification",
                    text: "Tất cả dữ liệu thuộc thông báo này sẽ bị xóa không thể khôi phục",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function () {
                    $.ajax({
                        'url': url_delete,
                        'success': function (data) {
                            if (data == 'ok') {
                                swal("Deleted!", "Xóa thông báo thành công", "success");
                                wraper.remove();
                            } else {
                                swal("Failed", "Xóa thông báo thất bại", "error");
                            }
                        }
                    });
                });
        });

        $('#select_all').change(function () {
            if ($(this).is(':checked'))
                $('.i_s').each(function () {
                    $(this).prop('checked', true);
                });
            else
                $('.i_s').each(function () {
                    $(this).prop('checked', false);
                });
        });

        $('#delete_all').click(function () {
            swal({
                    title: "Delete notification",
                    text: "Bạn muốn xoá tất cả thông báo đã chọn???",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function () {
                    $('.i_s').each(function () {
                        var url_delete = $(this).val();
                        var wraper = $(this).parents('tr');
                        if ($(this).is(":checked")) {
                            wraper.remove();
                            $.ajax({
                                'url': url_delete,
                                'success': function (data) {
                                    swal("Deleted!", "Xóa thông báo thành công", "success");
                                }
                            });
                        }
                    });
                });
        })

    </script>
@stop