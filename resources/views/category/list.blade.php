@extends('layouts.dashboard')
@section('title', 'Quản lý chuyên mục')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table m-0 table-colored table-inverse">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên</th>
                        <th>Hình ảnh</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr class="main-cate">
                            <th scope="row">{{ $category->id }}</th>
                            <td>{{ $category->name }}</td>
                            <td><img src="{{ $category->icon }}" width="50px" height="50px" alt=""></td>
                            <td width="15%" class="text-right">
                                <a href="{{ route('category-edit', [$category->id]) }}"
                                   class="btn btn-sm btn-success"><i
                                            class="fa fa-edit"></i></a>
                                <a href="{{ route('category-delete', [$category->id]) }}"
                                   class="btn btn-sm btn-danger btn-delete-app"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @if($category->parent_id == 0)
                            @foreach($category->childs as $sub_cate)
                                <tr>
                                    <th scope="row">{{ $sub_cate->id }}</th>
                                    <td>{{ $sub_cate->name }}</td>
                                    <td><img src="{{ $sub_cate->icon }}" width="50px" height="50px" alt=""></td>
                                    <td width="15%" class="text-right">
                                        <a href="{{ route('category-edit', [$sub_cate->id]) }}"
                                           class="btn btn-sm btn-success"><i
                                                    class="fa fa-edit"></i></a>
                                        <a href="{{ route('category-delete', [$sub_cate->id]) }}"
                                           class="btn btn-sm btn-danger btn-delete-app"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


@section('css')
    <link rel="stylesheet" href="{{ url('resources/assets/plugin/amcharts/export.css') }}">
@stop

@section('script')
    <script type="text/javascript">
        $('.btn-delete-app').click(function (e) {
            e.preventDefault();
            var url_delete = $(this).attr('href');
            var wraper = $(this).parents('tr');
            swal({
                        title: "Xóa chuyên mục",
                        text: "Tất cả dữ liệu thuộc chuyên mục này sẽ bị xóa không thể khôi phục",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        $.ajax({
                            'url': url_delete,
                            'success': function (data) {
                                if (data == 'ok') {
                                    swal("Deleted!", "Xóa chuyên mục thành công", "success");
                                    wraper.remove();
                                } else {
                                    swal("Failed", "Xóa chuyên mục thất bại", "error");
                                }
                            }
                        });
                    });
        });
    </script>
@stop