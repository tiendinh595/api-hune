@extends('layouts.dashboard')
@section('title', 'Thêm chuyên mục')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                @include('includes.errors')
                @include('includes.success')
                <form role="form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Tên tiếng việt</label>
                            <input type="text" class="form-control" value="{{ old('name_vi') }}" id="name_vi"
                                   name="name_vi"
                                   placeholder="tên chuyên mục">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Tên tiếng anh</label>
                            <input type="text" class="form-control" value="{{ old('name_en') }}" id="name_en"
                                   name="name_en"
                                   placeholder="tên chuyên mục">
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="parent_id">Chuyên mục mẹ</label>
                            <select name="parent_id" class="form-control" id="parent_id">
                                <option value="0">Master</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="icon">Ảnh</label>
                            <input type="file" class="filestyle" data-buttonname="btn-primary" name="icon"
                                   id="filestyle-4"
                                   tabindex="-1" style="position: absolute; clip: rect(0px 0px 0px 0px);">
                            <div class="bootstrap-filestyle input-group"><input type="text"
                                                                                class="form-control "
                                                                                placeholder="" disabled="">
                                <span
                                        class="group-span-filestyle input-group-btn" tabindex="0"><label
                                            for="filestyle-4" class="btn btn-primary "><span
                                                class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span
                                                class="buttonText">Choose file</span></label></span></div>
                        </div>
                    </div>

                    <p class="text-center"><button type="submit" class="btn btn-info waves-effect waves-light">Save</button></p>
                </form>
            </div>
        </div>
        @stop

        @section('css')
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/switchery/switchery.min.css') }}">
            <link rel="stylesheet" href="{{ url('resources/assets/plugin/summernote/summernote.css') }}">
            <script src="{{ url('resources/assets/plugin/switchery/switchery.min.js') }}"></script>
        @stop

        @section('script')
            <script src="{{ url('resources/assets/plugin/summernote/summernote.min.js') }}"></script>
            <script src="{{ url('resources/assets/js/jquery.autocomplete.min.js') }}"></script>
            <script type="text/javascript">

                var el = document.querySelector('.js-switch');
                var init = new Switchery(el, {size: 'small'});

                $('#AppDescription').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                  // set focus to editable area after initializing summernote
                });
                $('#Publisher').autocomplete({
                    serviceUrl: '/publisher/search',
                    onSelect: function (suggestion) {
                        $('#PublisherID').val(suggestion.data)
                    }
                });

            </script>
@stop