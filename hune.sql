/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost
 Source Database       : hune

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : utf-8

 Date: 08/31/2017 14:10:41 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2017_07_23_055329_create_jobs_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `model_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(255) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_id` (`model_id`,`model_type`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `model_has_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `model_has_permissions` VALUES ('200', '5', 'App\\Admin', '18', null, null), ('201', '5', 'App\\Admin', '19', null, null), ('202', '5', 'App\\Admin', '20', null, null), ('203', '5', 'App\\Admin', '21', null, null), ('204', '5', 'App\\Admin', '22', null, null), ('205', '5', 'App\\Admin', '23', null, null), ('206', '5', 'App\\Admin', '24', null, null), ('207', '5', 'App\\Admin', '25', null, null), ('208', '5', 'App\\Admin', '26', null, null), ('209', '5', 'App\\Admin', '27', null, null), ('210', '5', 'App\\Admin', '28', null, null), ('211', '5', 'App\\Admin', '29', null, null), ('212', '5', 'App\\Admin', '30', null, null), ('213', '5', 'App\\Admin', '31', null, null), ('214', '5', 'App\\Admin', '32', null, null), ('215', '5', 'App\\Admin', '33', null, null), ('216', '5', 'App\\Admin', '34', null, null), ('217', '5', 'App\\Admin', '35', null, null), ('218', '5', 'App\\Admin', '36', null, null), ('219', '5', 'App\\Admin', '37', null, null), ('220', '5', 'App\\Admin', '38', null, null), ('221', '5', 'App\\Admin', '39', null, null), ('222', '5', 'App\\Admin', '40', null, null), ('223', '5', 'App\\Admin', '41', null, null), ('224', '5', 'App\\Admin', '42', null, null), ('225', '5', 'App\\Admin', '43', null, null), ('226', '5', 'App\\Admin', '44', null, null), ('227', '5', 'App\\Admin', '45', null, null), ('228', '5', 'App\\Admin', '46', null, null), ('229', '5', 'App\\Admin', '47', null, null), ('230', '5', 'App\\Admin', '48', null, null), ('231', '5', 'App\\Admin', '49', null, null), ('232', '5', 'App\\Admin', '50', null, null), ('233', '5', 'App\\Admin', '51', null, null), ('234', '5', 'App\\Admin', '52', null, null), ('390', '2', 'App\\Admin', '18', null, null), ('391', '2', 'App\\Admin', '19', null, null), ('392', '2', 'App\\Admin', '20', null, null), ('393', '2', 'App\\Admin', '21', null, null), ('394', '2', 'App\\Admin', '22', null, null), ('395', '2', 'App\\Admin', '23', null, null), ('396', '2', 'App\\Admin', '24', null, null), ('397', '2', 'App\\Admin', '25', null, null), ('398', '2', 'App\\Admin', '26', null, null), ('399', '2', 'App\\Admin', '27', null, null), ('400', '2', 'App\\Admin', '28', null, null), ('401', '2', 'App\\Admin', '29', null, null), ('402', '2', 'App\\Admin', '30', null, null), ('403', '2', 'App\\Admin', '31', null, null), ('404', '2', 'App\\Admin', '32', null, null), ('405', '2', 'App\\Admin', '33', null, null), ('406', '2', 'App\\Admin', '34', null, null), ('407', '2', 'App\\Admin', '35', null, null), ('408', '2', 'App\\Admin', '36', null, null), ('409', '2', 'App\\Admin', '37', null, null), ('410', '2', 'App\\Admin', '38', null, null), ('411', '2', 'App\\Admin', '39', null, null), ('412', '2', 'App\\Admin', '40', null, null), ('413', '2', 'App\\Admin', '41', null, null), ('414', '2', 'App\\Admin', '42', null, null), ('415', '2', 'App\\Admin', '43', null, null), ('416', '2', 'App\\Admin', '44', null, null), ('417', '2', 'App\\Admin', '45', null, null), ('418', '2', 'App\\Admin', '46', null, null), ('419', '2', 'App\\Admin', '47', null, null), ('420', '2', 'App\\Admin', '48', null, null), ('421', '2', 'App\\Admin', '49', null, null), ('422', '2', 'App\\Admin', '50', null, null), ('423', '2', 'App\\Admin', '51', null, null), ('424', '2', 'App\\Admin', '52', null, null), ('425', '6', 'App\\Admin', '18', null, null), ('426', '6', 'App\\Admin', '19', null, null), ('427', '6', 'App\\Admin', '20', null, null), ('428', '6', 'App\\Admin', '21', null, null), ('429', '6', 'App\\Admin', '22', null, null), ('430', '6', 'App\\Admin', '23', null, null), ('431', '6', 'App\\Admin', '24', null, null), ('432', '6', 'App\\Admin', '25', null, null), ('433', '6', 'App\\Admin', '26', null, null), ('434', '6', 'App\\Admin', '27', null, null), ('435', '6', 'App\\Admin', '28', null, null), ('436', '6', 'App\\Admin', '29', null, null), ('437', '6', 'App\\Admin', '30', null, null), ('438', '6', 'App\\Admin', '31', null, null), ('439', '6', 'App\\Admin', '32', null, null), ('440', '6', 'App\\Admin', '33', null, null), ('441', '6', 'App\\Admin', '34', null, null), ('442', '6', 'App\\Admin', '35', null, null), ('443', '6', 'App\\Admin', '36', null, null), ('444', '6', 'App\\Admin', '37', null, null), ('445', '6', 'App\\Admin', '38', null, null), ('446', '6', 'App\\Admin', '39', null, null), ('447', '6', 'App\\Admin', '40', null, null), ('448', '6', 'App\\Admin', '41', null, null), ('449', '6', 'App\\Admin', '42', null, null), ('450', '6', 'App\\Admin', '43', null, null), ('451', '6', 'App\\Admin', '44', null, null), ('452', '6', 'App\\Admin', '45', null, null), ('453', '6', 'App\\Admin', '46', null, null), ('454', '6', 'App\\Admin', '47', null, null), ('455', '6', 'App\\Admin', '48', null, null), ('456', '6', 'App\\Admin', '49', null, null), ('457', '6', 'App\\Admin', '50', null, null), ('458', '6', 'App\\Admin', '51', null, null), ('459', '6', 'App\\Admin', '52', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `model_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('18', 'admin', 'list category', 'category', 'list-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('19', 'admin', 'view category', 'category', 'view-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('20', 'admin', 'add category', 'category', 'add-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('21', 'admin', 'edit category', 'category', 'edit-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('22', 'admin', 'delete category', 'category', 'delete-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('23', 'admin', 'list post', 'post', 'list-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('24', 'admin', 'view post', 'post', 'view-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('25', 'admin', 'add post', 'post', 'add-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('26', 'admin', 'edit post', 'post', 'edit-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('27', 'admin', 'delete post', 'post', 'delete-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('28', 'admin', 'list notification', 'notification', 'list-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('29', 'admin', 'view notification', 'notification', 'view-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('30', 'admin', 'add notification', 'notification', 'add-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('31', 'admin', 'edit notification', 'notification', 'edit-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('32', 'admin', 'delete notification', 'notification', 'delete-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('33', 'admin', 'list user', 'user', 'list-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('34', 'admin', 'view user', 'user', 'view-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('35', 'admin', 'add user', 'user', 'add-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('36', 'admin', 'edit user', 'user', 'edit-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('37', 'admin', 'delete user', 'user', 'delete-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('38', 'admin', 'list admin', 'admin', 'list-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('39', 'admin', 'view admin', 'admin', 'view-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('40', 'admin', 'add admin', 'admin', 'add-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('41', 'admin', 'edit admin', 'admin', 'edit-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('42', 'admin', 'delete admin', 'admin', 'delete-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('43', 'admin', 'list permission', 'permission', 'list-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('44', 'admin', 'view permission', 'permission', 'view-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('45', 'admin', 'add permission', 'permission', 'add-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('46', 'admin', 'edit permission', 'permission', 'edit-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('47', 'admin', 'delete permission', 'permission', 'delete-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('48', 'admin', 'list comment', 'comment', 'list-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('49', 'admin', 'view comment', 'comment', 'view-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('50', 'admin', 'add comment', 'comment', 'add-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('51', 'admin', 'edit comment', 'comment', 'edit-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('52', 'admin', 'delete comment', 'comment', 'delete-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41');
COMMIT;

-- ----------------------------
--  Table structure for `role_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_admin`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_admin` VALUES ('2', 'Dinh Vu', 'dinh@flychicken.net', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'dinh@flychicken.net', '$2y$10$6udrChjYviIf9EIpwzm6DuFf7FUX97O0XNTUV2mgp16FFnsKuP5he', 'Z9WzH9LZeEsNPFliOlKiOICprMrSUEhjAK1YIRJBSO6UVO7gpkaGX2xo0mEP', null, '2017-08-29 09:45:47'), ('5', 'VU TIEN DINH', 'tiendinh595@gmail.com', null, 'tiendinh595@gmail.com', '$2y$10$OBhVBk6bdLiGP6CNOhO29.z3HykHDkdVcnndgPUgl4zE/fmyAScti', null, '2017-08-14 14:43:58', '2017-08-14 15:32:05'), ('6', 'dinh nguyen', 'dinhnguyeniuh@gmail.com', null, 'dinhnguyeniuh@gmail.com', '$2y$10$3e8irbuQ3sZSMaBwmNsDFu0FpYZbbSA1mn0UAnTRcmuCrGmqGuo7a', null, '2017-08-31 14:10:11', '2017-08-31 14:10:11');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_category`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_category`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_category` VALUES ('1', 'làm đẹp', null, '0', null, '2017-07-30 14:33:12'), ('2', 'Nhân viên', null, '0', null, null), ('3', 'Dịch vụ', null, '0', null, null), ('4', 'Lao động', null, '0', null, null), ('6', 'child1', '2017/07/30/1501428985-child1.png', '1', null, '2017-07-30 15:36:25');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_favourite`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_favourite`;
CREATE TABLE `tbl_favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type_post` tinyint(1) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: user, 2: post',
  `source_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`source_id`),
  KEY `user_id_2` (`user_id`),
  KEY `type` (`type`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_favourite`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_favourite` VALUES ('2', '1', '1', '1', '2', '2017-07-22 07:30:24', '2017-07-22 07:30:24'), ('4', '20', '1', '1', '2', '2017-08-02 10:03:55', '2017-08-02 10:03:55');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notification`;
CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `chanel` tinyint(1) DEFAULT '1' COMMENT '1: broadcast, 2: private',
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Enrollment, 2: search job, 3: system',
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `user_id` (`user_id`),
  KEY `chanel` (`chanel`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_notification`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_notification` VALUES ('2', 'testttttt', 'thiss i content', '1', '1', null, '2017-07-21 15:45:22', null), ('3', 'tesst', 'ok', '1', '1', null, '2017-08-01 05:00:43', '2017-08-01 05:00:43'), ('4', 'tesst', 'test noti', '1', '1', null, '2017-08-01 10:20:10', '2017-08-01 10:20:10'), ('5', 'tesst', 'test noti', '1', '1', null, '2017-08-01 10:34:16', '2017-08-01 10:34:16'), ('6', 'tesst', 'test noti', '1', '1', null, '2017-08-01 10:34:21', '2017-08-01 10:34:21'), ('7', 'tesst', 'test noti', '1', '1', null, '2017-08-01 10:38:35', '2017-08-01 10:38:35'), ('8', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:46:44', '2017-08-01 12:46:44'), ('9', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:48:02', '2017-08-01 12:48:02'), ('10', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:49:00', '2017-08-01 12:49:00'), ('11', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:49:37', '2017-08-01 12:49:37'), ('12', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:50:11', '2017-08-01 12:50:11'), ('13', 'tesst', 'tesst', '1', '1', null, '2017-08-01 12:50:45', '2017-08-01 12:50:45'), ('14', 'tesst ada', 'dddddadasda', '1', '1', null, '2017-08-01 12:51:05', '2017-08-01 12:57:39');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_notification_read`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notification_read`;
CREATE TABLE `tbl_notification_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_id` (`notification_id`,`user_id`),
  KEY `notification_id_2` (`notification_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `tbl_post`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_post`;
CREATE TABLE `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `salary` decimal(10,0) DEFAULT NULL,
  `salary_type` tinyint(4) DEFAULT '1' COMMENT '1: hour, 2: day, 3: week, 4: month, 5: year',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: on, 2: off',
  `sex` tinyint(1) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Enrollment, 2: search job',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`category_id`,`type`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `category_parent_id` (`category_parent_id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_post`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_post` VALUES ('3', '2', '1', '6', 'nhiếp ảnh', '[\"17\\/07\\/22\\/1500717754.png\"]', '17/07/22/1500717754.png', '2', '1', 'hcm', '10.7818', '106.635', null, null, null, null, 'jenkins', '1', null, '2', '2017-07-22 10:02:34', '2017-07-31 15:46:02'), ('4', '20', '1', '6', 'tuyen dev php', '[]', null, '2', '1', 'hcm', '1', '2', '5000000', '4', null, null, 'dev php', '1', '1', '1', '2017-08-03 13:25:41', '2017-08-09 03:55:32'), ('5', '11', '1', '6', 'tesst add post', '[]', null, null, '0', null, null, null, null, null, null, null, 'mo ta', '1', '1', '2', '2017-08-12 16:16:24', '2017-08-12 16:16:24'), ('6', '8', '1', '6', 'abc', '[\"2017\\/08\\/12\\/1502554718.jpeg\",\"2017\\/08\\/12\\/1502554719.jpeg\"]', '2017/08/12/1502554718.jpeg', null, '0', null, null, null, null, null, null, null, 'abc', '1', '1', '2', '2017-08-12 16:18:39', '2017-08-12 16:18:39'), ('7', '2', '1', '6', 'dadad', '{\"0\":\"2017\\/08\\/28\\/1503896066.png\",\"2\":\"\\/\\/\\/\\/\\/2017\\/08\\/24\\/1503561824.jpeg\",\"1\":\"\\/2017\\/08\\/28\\/1503895857.png\"}', '', '0', '1', 'hcm', null, null, '100', '1', null, null, 'dadd', '1', '1', '1', '2017-08-24 03:37:53', '2017-08-28 11:54:26');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_post_media`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_post_media`;
CREATE TABLE `tbl_post_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `tbl_report`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_report`;
CREATE TABLE `tbl_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `tbl_saved_post`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_saved_post`;
CREATE TABLE `tbl_saved_post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '1' COMMENT '1: female, 2:male',
  `full_name` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `fcm_token` text,
  `token` text,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `active_code` varchar(10) DEFAULT NULL,
  `rating` double DEFAULT '0',
  `favourite_count` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '1:active, 2: verified, 3: banned',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_id` (`facebook_id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_user`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES ('2', null, '$2y$10$srYDaTjFvT4K1OMGuM/.levRSLmnyAxi/cqkWsK/oIo9M5MVwK8EG', null, '01672010030', '1', 'VU TIEN DINH', null, null, null, null, null, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9odW5lLmRldi9hcGkvdjEvdXNlci9sb2dpbi1hY2NvdW50IiwiaWF0IjoxNTAyNDQxMjE1LCJleHAiOjE2NTc5NjEyMTUsIm5iZiI6MTUwMjQ0MTIxNSwianRpIjoiSWpJd2ttRFI1WUQ1T2NkUSJ9.KIKbu9RCGoEH-coEy3Lth7IWkh0ckg7nLHDlGwojXgI', null, null, '3', '1', '1', '2017-07-21 05:45:18', '2017-08-11 08:46:55'), ('3', null, '$2y$10$fdt7Kz/JtIW6Akymly4h2O/SiR4RV/l6GS1QDrDeH/Oe9E.IuY71e', null, '016720100303', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '8850', '0', '0', '1', '2017-07-23 05:50:28', '2017-07-23 05:50:28'), ('4', null, '$2y$10$3cgvHY/hLs9ll0DwvMbbj.ta27HGnVFoEogIwG6xwkWUcrzTT7l7u', null, '016720100305', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '6477', '0', '0', '1', '2017-07-23 05:50:55', '2017-07-23 05:50:55'), ('5', null, '$2y$10$xTy8LRx4eat02sJiT3wPUeNvxbrUzGRXJEVsryRIbhaqOdn7h6JQu', null, '016720100306', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '5329', '0', '0', '1', '2017-07-23 05:56:31', '2017-07-23 05:56:31'), ('6', null, '$2y$10$9a53tGcsA0uaLC2/9nfbhu3JQWLQYGm0VAt9yo0sidQtIvYCjkwRK', null, '016720100309', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '1016', '0', '0', '1', '2017-07-23 06:03:58', '2017-07-23 06:03:58'), ('7', null, '$2y$10$8hRCCA9.fYC4ezJSs7VLIuwcj6q2l9ieHempT.HgGftKsPTmmkCxO', null, '016720100301', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '7763', '0', '0', '1', '2017-07-23 06:06:48', '2017-07-23 06:06:48'), ('8', null, '$2y$10$ESb.6tafEjHfNo39hNvAJOJ67nbR0KAELENtKZn7RHp1pR4dqwaj.', null, '0167201003032', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '7856', '0', '0', '1', '2017-07-23 06:08:00', '2017-07-23 06:08:00'), ('9', null, '$2y$10$EhKv4Tg5EjOoba9moRHkv.3E6M67t44O5ZL4xi9NbsjR6VDDc27Nm', null, '0167201003031', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '3179', '0', '0', '1', '2017-07-23 06:08:31', '2017-07-23 06:08:31'), ('10', null, '$2y$10$Qq0kaVNegGtSyiatOexJN.vnmIl9bmuWKau2/UjFgH2jxEggwefoa', null, '01672010030313', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '7976', '0', '0', '1', '2017-07-23 06:08:55', '2017-07-23 06:08:55'), ('11', null, '$2y$10$Iam.ASv8hmLHXY2EMbLy/uuGxbe45AlKcR2PnLJcg/OtTOaFnCQJS', null, '016720100303132', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '6448', '0', '0', '1', '2017-07-23 06:10:51', '2017-07-23 06:10:51'), ('12', null, '$2y$10$SVt8FZL.nF5yt8i02Lv3LerwaUnPO9CdnolhWPhTvESlCIozUzcnS', null, '016720100303134', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '2326', '0', '0', '1', '2017-07-23 06:11:49', '2017-07-23 06:11:49'), ('13', null, '$2y$10$EYpfd1Ft4yk4szwdD8GarONt1oKTnsqo8Xf6HDhbJJjIqI.jA87b6', null, '016720100303133', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '4035', '0', '0', '1', '2017-07-23 06:16:52', '2017-07-23 06:16:52'), ('14', null, '$2y$10$XH6AkTrVkTO9YhhlShbuLutNbZwu760c38btP3GoyjNdFyFU1FxP2', null, '016720103', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '9575', '0', '0', '1', '2017-07-23 06:17:24', '2017-07-23 06:17:24'), ('15', null, '$2y$10$8dqIdTh.TOyVi9NgPMt78.o7nOqcvar2CR43YIDiNGnRwF3cJknuq', null, '0167201037', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '5748', '0', '0', '1', '2017-07-23 06:18:28', '2017-07-23 06:18:28'), ('16', null, '$2y$10$d/Dl/Uc50j.ZfB6QpWomE.OhN73YfCHwDUoedo4OarLFAhbsmmZhG', null, '01672010378', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '9858', '0', '0', '1', '2017-07-23 06:19:40', '2017-07-23 06:19:40'), ('17', null, '$2y$10$pUJRdri2XPWjaFkC3h8zM./4dDc9.bvrjsclQTUN4zJxdy1uH9KCy', null, '01672010374', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '6490', '0', '0', '1', '2017-07-23 06:21:17', '2017-07-23 06:21:17'), ('18', null, '$2y$10$jTukZ4WxZVmzAXA6L.4wTeA48kZKZ3iP77vdae9OeXvDElsExJ2Iu', null, '01672010375', '1', 'VU TIEN DINH', null, null, null, null, null, null, null, '5531', '0', '0', '1', '2017-07-23 06:22:40', '2017-07-23 06:22:40'), ('20', null, '$2y$10$voIJotGpJWzXAsnZB7/IwOyklI1OPwebhmdEUIuTKGx9bvtRdumvC', '745878638906068', null, '1', 'Vũ Tiến Định', null, 'https://scontent.xx.fbcdn.net/v/t1.0-1/p320x320/18767391_718724814954784_1411376340095130213_n.jpg?oh=6f3c842f3c8b25351363840eee0d0694&oe=59FB62AF', null, null, '1111', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwLCJpc3MiOiJodHRwOi8vaHVuZS5kZXYvYXBpL3YxL3VzZXIvbG9naW4tZmFjZWJvb2siLCJpYXQiOjE1MDA3OTE5ODcsImV4cCI6MTY1NjMxMTk4NywibmJmIjoxNTAwNzkxOTg3LCJqdGkiOiJRZFZxbk9oTU1NdHVNR3VEIn0.9biFi__Hl73pnFj6HgRnO5aAHfT-9xPnV-R1YyX7YTY', null, null, '0', '0', '2', '2017-07-23 06:38:36', '2017-07-23 06:39:47'), ('21', null, '$2y$10$lKCzaAjOAQHodh9kSf4RfOPELCYhu9uulo9KuJFW/gKTZGJskC.mu', null, '0964328327', '2', 'Vu Tien Dinh', null, null, 'developer', 'hcm', null, null, null, '', '0', '0', '2', '2017-07-27 13:50:21', '2017-07-31 14:45:35'), ('22', null, '$2y$10$3wzU7DykXTvzwIkZeI5hmeKKwmPi2tZsXPL9n1IT0n8PPviWDUWz2', null, '09090909', '2', 'VU TIEN DINH', null, '2017/08/02/1501649408.jpg', null, null, '1111', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyLCJpc3MiOiJodHRwOi8vaHVuZS5kZXYvYXBpL3YxL3VzZXIvbG9naW4tYWNjb3VudCIsImlhdCI6MTUwMTY0OTQ1MywiZXhwIjoxNjU3MTY5NDUzLCJuYmYiOjE1MDE2NDk0NTMsImp0aSI6ImRQakZackQ0WU9hVGZ1YTUifQ.gwa9OsVdxc4mZbRny2IRFhGS_Teic-RBBCJccMWoecc', null, '6648', '3', '0', '1', '2017-08-02 04:50:08', '2017-08-03 14:35:36'), ('23', null, '$2y$10$R4FS/ViZALV8aGzUpOBq0ei87.Gtm7ClJAKgvFl3RPloHkfL3NbBq', null, '0909090909', '1', 'VU TIEN DINH', null, '2017/08/07/1502080094.jpg', null, null, null, null, null, '2785', '0', '0', '1', '2017-08-07 04:28:14', '2017-08-07 04:28:14'), ('24', null, '$2y$10$iFSsc0XSTW26Ki1PbXNVXe1H84d974SMaq.BARnFTvb50Pp.l/HY6', '1169324399833735', null, '1', 'Hồ Quốc Hưng', null, 'https://scontent.xx.fbcdn.net/v/t1.0-1/p320x320/18447488_1089903664442476_3638965381705152730_n.jpg?oh=0673fcb3f4985fbac7f237083e2fbd62&oe=5A309881', null, null, null, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI0LCJpc3MiOiJodHRwOi8vaHVuZS5kZXYvYXBpL3YxL3VzZXIvbG9naW4tZmFjZWJvb2siLCJpYXQiOjE1MDI4NTkwOTksImV4cCI6MTY1ODM3OTA5OSwibmJmIjoxNTAyODU5MDk5LCJqdGkiOiJGOEVNdG1uaE9rUEhJbFloIn0.d0oTLaaOMg4-jhHThwFB59QP60S_S0xb33NgOz1E6z4', null, null, '0', '0', '1', '2017-08-16 04:51:39', '2017-08-16 04:51:39');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_user_media`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_media`;
CREATE TABLE `tbl_user_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `tbl_vote`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_vote`;
CREATE TABLE `tbl_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: on, 2: off',
  `type` tinyint(4) DEFAULT '1' COMMENT '1: user, 2: post',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`source_id`,`type`),
  KEY `source_id` (`source_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_vote`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_vote` VALUES ('1', '3', '2', '1', 'tests', '2', '1', '2017-07-22 05:34:49', '2017-08-10 14:01:58'), ('3', '2', '4', '3', 'this is comment', '1', '1', null, '2017-08-12 07:05:57'), ('4', '5', '3', '5', 'this is comment', '1', '2', '2017-07-22 15:47:41', '2017-07-22 15:47:41'), ('5', '20', '2', '3', 'test', '1', '1', '2017-08-03 13:53:22', '2017-08-03 13:53:22');
COMMIT;

-- ----------------------------
--  Table structure for `user_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `user_has_permissions`;
CREATE TABLE `user_has_permissions` (
  `user_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'Dinh Vu', 'dinh@flychicken.net', '$2y$10$cT87OtQMoRxxpTt3SvxgkOkZiSZZbSdIPEVUsnylA0i61OKBm2ul2', 'dinh@flychicken.net', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'q5m5cawIVWUkNL6MsQCICXwFaBD9WJEC176ud5jsTXgx1LyIdXU0nIiM4Vg2', null, '2017-08-11 08:06:08'), ('2', 'dinh', 'dinhvt@hip.vn', '$2y$10$i8VergplWqRRBjHXv9YKJO0hdcjZnlxZec37cl4GzWfjt7pRFC.6i', 'dinhvt@hip.vn', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', null, '2017-07-19 14:41:08', '2017-07-19 14:41:08'), ('3', 'Tam Trinh', 'tam@flychicken.net', '$2y$10$OWQDmYZp1EGXQR0VBOVNxu0fEJ9.WLMHvGUrhs/buQDe6Quj6J61G', 'tam@flychicken.net', 'https://lh4.googleusercontent.com/-TK8Bpu_ZQU0/AAAAAAAAAAI/AAAAAAAAAA0/tWhSG-ZLYGU/photo.jpg?sz=50', null, '2017-07-19 14:41:08', '2017-08-11 09:25:38');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
