/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost
 Source Database       : fct_tamsu

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : utf-8

 Date: 09/22/2017 14:23:34 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `model_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(255) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_id` (`model_id`,`model_type`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `model_has_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `model_has_permissions` VALUES ('200', '5', 'App\\Admin', '18', null, null), ('201', '5', 'App\\Admin', '19', null, null), ('202', '5', 'App\\Admin', '20', null, null), ('203', '5', 'App\\Admin', '21', null, null), ('204', '5', 'App\\Admin', '22', null, null), ('205', '5', 'App\\Admin', '23', null, null), ('206', '5', 'App\\Admin', '24', null, null), ('207', '5', 'App\\Admin', '25', null, null), ('208', '5', 'App\\Admin', '26', null, null), ('209', '5', 'App\\Admin', '27', null, null), ('210', '5', 'App\\Admin', '28', null, null), ('211', '5', 'App\\Admin', '29', null, null), ('212', '5', 'App\\Admin', '30', null, null), ('213', '5', 'App\\Admin', '31', null, null), ('214', '5', 'App\\Admin', '32', null, null), ('215', '5', 'App\\Admin', '33', null, null), ('216', '5', 'App\\Admin', '34', null, null), ('217', '5', 'App\\Admin', '35', null, null), ('218', '5', 'App\\Admin', '36', null, null), ('219', '5', 'App\\Admin', '37', null, null), ('220', '5', 'App\\Admin', '38', null, null), ('221', '5', 'App\\Admin', '39', null, null), ('222', '5', 'App\\Admin', '40', null, null), ('223', '5', 'App\\Admin', '41', null, null), ('224', '5', 'App\\Admin', '42', null, null), ('225', '5', 'App\\Admin', '43', null, null), ('226', '5', 'App\\Admin', '44', null, null), ('227', '5', 'App\\Admin', '45', null, null), ('228', '5', 'App\\Admin', '46', null, null), ('229', '5', 'App\\Admin', '47', null, null), ('230', '5', 'App\\Admin', '48', null, null), ('231', '5', 'App\\Admin', '49', null, null), ('232', '5', 'App\\Admin', '50', null, null), ('233', '5', 'App\\Admin', '51', null, null), ('234', '5', 'App\\Admin', '52', null, null), ('390', '2', 'App\\Admin', '18', null, null), ('391', '2', 'App\\Admin', '19', null, null), ('392', '2', 'App\\Admin', '20', null, null), ('393', '2', 'App\\Admin', '21', null, null), ('394', '2', 'App\\Admin', '22', null, null), ('395', '2', 'App\\Admin', '23', null, null), ('396', '2', 'App\\Admin', '24', null, null), ('397', '2', 'App\\Admin', '25', null, null), ('398', '2', 'App\\Admin', '26', null, null), ('399', '2', 'App\\Admin', '27', null, null), ('400', '2', 'App\\Admin', '28', null, null), ('401', '2', 'App\\Admin', '29', null, null), ('402', '2', 'App\\Admin', '30', null, null), ('403', '2', 'App\\Admin', '31', null, null), ('404', '2', 'App\\Admin', '32', null, null), ('405', '2', 'App\\Admin', '33', null, null), ('406', '2', 'App\\Admin', '34', null, null), ('407', '2', 'App\\Admin', '35', null, null), ('408', '2', 'App\\Admin', '36', null, null), ('409', '2', 'App\\Admin', '37', null, null), ('410', '2', 'App\\Admin', '38', null, null), ('411', '2', 'App\\Admin', '39', null, null), ('412', '2', 'App\\Admin', '40', null, null), ('413', '2', 'App\\Admin', '41', null, null), ('414', '2', 'App\\Admin', '42', null, null), ('415', '2', 'App\\Admin', '43', null, null), ('416', '2', 'App\\Admin', '44', null, null), ('417', '2', 'App\\Admin', '45', null, null), ('418', '2', 'App\\Admin', '46', null, null), ('419', '2', 'App\\Admin', '47', null, null), ('420', '2', 'App\\Admin', '48', null, null), ('421', '2', 'App\\Admin', '49', null, null), ('422', '2', 'App\\Admin', '50', null, null), ('423', '2', 'App\\Admin', '51', null, null), ('424', '2', 'App\\Admin', '52', null, null), ('425', '6', 'App\\Admin', '18', null, null), ('426', '6', 'App\\Admin', '19', null, null), ('427', '6', 'App\\Admin', '20', null, null), ('428', '6', 'App\\Admin', '21', null, null), ('429', '6', 'App\\Admin', '22', null, null), ('430', '6', 'App\\Admin', '23', null, null), ('431', '6', 'App\\Admin', '24', null, null), ('432', '6', 'App\\Admin', '25', null, null), ('433', '6', 'App\\Admin', '26', null, null), ('434', '6', 'App\\Admin', '27', null, null), ('435', '6', 'App\\Admin', '28', null, null), ('436', '6', 'App\\Admin', '29', null, null), ('437', '6', 'App\\Admin', '30', null, null), ('438', '6', 'App\\Admin', '31', null, null), ('439', '6', 'App\\Admin', '32', null, null), ('440', '6', 'App\\Admin', '33', null, null), ('441', '6', 'App\\Admin', '34', null, null), ('442', '6', 'App\\Admin', '35', null, null), ('443', '6', 'App\\Admin', '36', null, null), ('444', '6', 'App\\Admin', '37', null, null), ('445', '6', 'App\\Admin', '38', null, null), ('446', '6', 'App\\Admin', '39', null, null), ('447', '6', 'App\\Admin', '40', null, null), ('448', '6', 'App\\Admin', '41', null, null), ('449', '6', 'App\\Admin', '42', null, null), ('450', '6', 'App\\Admin', '43', null, null), ('451', '6', 'App\\Admin', '44', null, null), ('452', '6', 'App\\Admin', '45', null, null), ('453', '6', 'App\\Admin', '46', null, null), ('454', '6', 'App\\Admin', '47', null, null), ('455', '6', 'App\\Admin', '48', null, null), ('456', '6', 'App\\Admin', '49', null, null), ('457', '6', 'App\\Admin', '50', null, null), ('458', '6', 'App\\Admin', '51', null, null), ('459', '6', 'App\\Admin', '52', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `model_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('18', 'admin', 'list category', 'category', 'list-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('19', 'admin', 'view category', 'category', 'view-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('20', 'admin', 'add category', 'category', 'add-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('21', 'admin', 'edit category', 'category', 'edit-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('22', 'admin', 'delete category', 'category', 'delete-category', '2017-08-14 14:07:30', '2017-08-14 14:07:30'), ('23', 'admin', 'list post', 'post', 'list-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('24', 'admin', 'view post', 'post', 'view-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('25', 'admin', 'add post', 'post', 'add-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('26', 'admin', 'edit post', 'post', 'edit-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('27', 'admin', 'delete post', 'post', 'delete-post', '2017-08-14 14:08:36', '2017-08-14 14:08:36'), ('28', 'admin', 'list notification', 'notification', 'list-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('29', 'admin', 'view notification', 'notification', 'view-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('30', 'admin', 'add notification', 'notification', 'add-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('31', 'admin', 'edit notification', 'notification', 'edit-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('32', 'admin', 'delete notification', 'notification', 'delete-notification', '2017-08-14 14:10:05', '2017-08-14 14:10:05'), ('33', 'admin', 'list user', 'user', 'list-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('34', 'admin', 'view user', 'user', 'view-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('35', 'admin', 'add user', 'user', 'add-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('36', 'admin', 'edit user', 'user', 'edit-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('37', 'admin', 'delete user', 'user', 'delete-user', '2017-08-14 14:10:11', '2017-08-14 14:10:11'), ('38', 'admin', 'list admin', 'admin', 'list-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('39', 'admin', 'view admin', 'admin', 'view-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('40', 'admin', 'add admin', 'admin', 'add-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('41', 'admin', 'edit admin', 'admin', 'edit-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('42', 'admin', 'delete admin', 'admin', 'delete-admin', '2017-08-14 14:10:15', '2017-08-14 14:10:15'), ('43', 'admin', 'list permission', 'permission', 'list-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('44', 'admin', 'view permission', 'permission', 'view-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('45', 'admin', 'add permission', 'permission', 'add-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('46', 'admin', 'edit permission', 'permission', 'edit-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('47', 'admin', 'delete permission', 'permission', 'delete-permission', '2017-08-14 14:10:24', '2017-08-14 14:10:24'), ('48', 'admin', 'list comment', 'comment', 'list-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('49', 'admin', 'view comment', 'comment', 'view-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('50', 'admin', 'add comment', 'comment', 'add-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('51', 'admin', 'edit comment', 'comment', 'edit-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41'), ('52', 'admin', 'delete comment', 'comment', 'delete-comment', '2017-08-14 14:10:41', '2017-08-14 14:10:41');
COMMIT;

-- ----------------------------
--  Table structure for `role_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_admin`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_admin` VALUES ('2', 'Dinh Vu', 'dinh@flychicken.net', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50', 'dinh@flychicken.net', '$2y$10$9/ph53ZDM.EN./Qt8q70VOHH01hB..F2IXQEZ4V7Y/4Srrp/xfT/2', 'Z9WzH9LZeEsNPFliOlKiOICprMrSUEhjAK1YIRJBSO6UVO7gpkaGX2xo0mEP', null, '2017-09-21 09:07:06'), ('5', 'VU TIEN DINH', 'tiendinh595@gmail.com', null, 'tiendinh595@gmail.com', '$2y$10$OBhVBk6bdLiGP6CNOhO29.z3HykHDkdVcnndgPUgl4zE/fmyAScti', null, '2017-08-14 14:43:58', '2017-08-14 15:32:05'), ('6', 'dinh nguyen', 'dinhnguyeniuh@gmail.com', null, 'dinhnguyeniuh@gmail.com', '$2y$10$3e8irbuQ3sZSMaBwmNsDFu0FpYZbbSA1mn0UAnTRcmuCrGmqGuo7a', null, '2017-08-31 14:10:11', '2017-08-31 14:10:11');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_chat`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_chat`;
CREATE TABLE `tbl_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `media` varchar(255) DEFAULT NULL,
  `media_type` tinyint(1) DEFAULT NULL COMMENT '1: image, 2: video',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_chat`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_chat` VALUES ('1', '1', '2', 'hi', null, null, '2017-09-22 09:14:33', null), ('2', '2', '1', 'xin chao', null, null, '2017-09-22 09:14:51', null), ('3', '3', '1', 'hello', null, null, '2017-09-22 09:15:08', null), ('4', '2', '1', 'what are you doing', null, null, '2017-09-22 09:20:02', null), ('5', '1', '2', 'wtf', null, null, '2017-09-22 09:45:31', null);
COMMIT;

-- ----------------------------
--  Table structure for `tbl_comic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_comic`;
CREATE TABLE `tbl_comic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `background` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_comment`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_comment`;
CREATE TABLE `tbl_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `post_type` tinyint(1) DEFAULT NULL COMMENT '1: confidential, 2 comic, 3: anonymous',
  `status` tinyint(1) DEFAULT '1' COMMENT '1: active, 2: inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_confidential`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_confidential`;
CREATE TABLE `tbl_confidential` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `background` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_confidential`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_confidential` VALUES ('1', 'trong tinh yeu', 'this is content', '2017/09/09/1.png', '2017-09-21 09:42:21', '2017-09-21 09:42:25');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notification`;
CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `chanel` tinyint(1) DEFAULT '1' COMMENT '1: broadcast, 2: private',
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `chanel` (`chanel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `tbl_notification`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_notification` VALUES ('2', 'testttttt', 'thiss i content', '1', null, '2017-07-21 15:45:22', null), ('3', 'tesst', 'ok', '1', null, '2017-08-01 05:00:43', '2017-08-01 05:00:43');
COMMIT;

-- ----------------------------
--  Table structure for `tbl_post_anonymous`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_post_anonymous`;
CREATE TABLE `tbl_post_anonymous` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `background` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_relationship`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_relationship`;
CREATE TABLE `tbl_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: pendding, 2: accepted, 3: reject',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`,`destination_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '1' COMMENT '1: female, 2:male',
  `phone` varchar(30) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL COMMENT '1: signle, 2: married, 3: divorced, 4: widowed ',
  `address` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `token` text,
  `fcm_token` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: active, 2: inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_user`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES ('1', null, null, 'dinh', null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, null), ('2', null, null, 'john', null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, null), ('3', null, null, 'siro', null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `user_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `user_has_permissions`;
CREATE TABLE `user_has_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_id` (`admin_id`,`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user_has_permissions`
-- ----------------------------
BEGIN;
INSERT INTO `user_has_permissions` VALUES ('390', '2', '18'), ('391', '2', '19'), ('392', '2', '20'), ('393', '2', '21'), ('394', '2', '22'), ('395', '2', '23'), ('396', '2', '24'), ('397', '2', '25'), ('398', '2', '26'), ('399', '2', '27'), ('400', '2', '28'), ('401', '2', '29'), ('402', '2', '30'), ('403', '2', '31'), ('404', '2', '32'), ('405', '2', '33'), ('406', '2', '34'), ('407', '2', '35'), ('408', '2', '36'), ('409', '2', '37'), ('410', '2', '38'), ('411', '2', '39'), ('412', '2', '40'), ('413', '2', '41'), ('414', '2', '42'), ('415', '2', '43'), ('416', '2', '44'), ('417', '2', '45'), ('418', '2', '46'), ('419', '2', '47'), ('420', '2', '48'), ('421', '2', '49'), ('422', '2', '50'), ('423', '2', '51'), ('424', '2', '52'), ('200', '5', '18'), ('201', '5', '19'), ('202', '5', '20'), ('203', '5', '21'), ('204', '5', '22'), ('205', '5', '23'), ('206', '5', '24'), ('207', '5', '25'), ('208', '5', '26'), ('209', '5', '27'), ('210', '5', '28'), ('211', '5', '29'), ('212', '5', '30'), ('213', '5', '31'), ('214', '5', '32'), ('215', '5', '33'), ('216', '5', '34'), ('217', '5', '35'), ('218', '5', '36'), ('219', '5', '37'), ('220', '5', '38'), ('221', '5', '39'), ('222', '5', '40'), ('223', '5', '41'), ('224', '5', '42'), ('225', '5', '43'), ('226', '5', '44'), ('227', '5', '45'), ('228', '5', '46'), ('229', '5', '47'), ('230', '5', '48'), ('231', '5', '49'), ('232', '5', '50'), ('233', '5', '51'), ('234', '5', '52'), ('425', '6', '18'), ('426', '6', '19'), ('427', '6', '20'), ('428', '6', '21'), ('429', '6', '22'), ('430', '6', '23'), ('431', '6', '24'), ('432', '6', '25'), ('433', '6', '26'), ('434', '6', '27'), ('435', '6', '28'), ('436', '6', '29'), ('437', '6', '30'), ('438', '6', '31'), ('439', '6', '32'), ('440', '6', '33'), ('441', '6', '34'), ('442', '6', '35'), ('443', '6', '36'), ('444', '6', '37'), ('445', '6', '38'), ('446', '6', '39'), ('447', '6', '40'), ('448', '6', '41'), ('449', '6', '42'), ('450', '6', '43'), ('451', '6', '44'), ('452', '6', '45'), ('453', '6', '46'), ('454', '6', '47'), ('455', '6', '48'), ('456', '6', '49'), ('457', '6', '50'), ('458', '6', '51'), ('459', '6', '52');
COMMIT;

-- ----------------------------
--  Table structure for `user_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_has_roles`;
CREATE TABLE `user_has_roles` (
  `admin_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
