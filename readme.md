# setup #

### database ###
* config/database.php

### cache ###
* chmod 0777 -R storage/framework
* chmod 0777 -R /bootstrap/cache

### url ###
config/app.php

### nginx ###
location / {
    try_files $uri $uri/ /index.php?$query_string;
}

### run worker ###
php artisan queue:work --queue="send-active-code" --daemon --tries=4# API-HUNE
