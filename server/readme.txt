event:
- emmit:
+ join: {'user_id': xxx, token: xxx} -> callback: {'code': xxx, msg: xxx}
+ send_message: {sender_id: xxx, receiver_id: xxx, message: xxx, media: xxx, media_type:1|2} -> callback:  {'code': xxx, msg: xxx}

- on:
+ send_message: {sender_id: xxx, receiver_id: xxx, message: xxx, media: xxx, media_type:1|2}