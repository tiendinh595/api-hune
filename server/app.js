// app.js
let express = require('express');
let app = express();
let server = require('http').createServer(app);
let io = require('socket.io')(server);
let fetch = require('node-fetch');
let api_url = 'http://tamsu.dev/api/v1';

io.sockets.on('connection', function (socket) {

    console.log("connected")
    socket.on('join', function (data, callback) {
        socket.join(data.user_id);
        socket._token = data.token;

        fetch(api_url+'/user/validate-token?token=' + data.token)
            .then(res => res.json())
            .then(function (data) {
                socket.is_auth = (data.code == 200) ? true : false;
                callback(data)
            })
            .catch(function (err) {
                callback({
                    "code": 500,
                    "msg": "Lỗi khi xác thực"
                });
            })
    });

    socket.on('send_message', function (data, callback) {
        try {
            if (!socket.is_auth) {
                fetch(api_url+'/chat?token=' + socket._token, {
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers: {'Content-Type': 'application/json'},
                }).then(res => res.json())
                    .then(function (res) {
                        if (res.code == 200) {
                            io.sockets.in(data.receiver_id).emit("send_message", data);
                            callback(res);
                        }
                        else
                            callback(res);
                    })
                    .catch(function (err) {
                        console.log(err)
                        callback({'code': 500, msg: 'Không gưi được tin nhắn'});
                    });
            } else {
                callback({'code': 500, msg: 'Xac thực thất bại'});
            }
        } catch (err) {
            callback({'code': 500, msg: 'Không gưi được tin nhắn'});
        }
    })
});

server.listen(3000, function () {
    console.log('server listen on port 3000')
});