<?php

namespace App\Console\Commands;

use App\Jobs\PushNotificationPost;
use App\Notification;
use App\Post;
use Illuminate\Console\Command;

class UpdatePostExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_post_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update all post expired and send notification to owner post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::where('end_date', date('Y-m-d', strtotime('-1 days')))->select(['id', 'user_id', 'title'])->get();
        foreach ($posts as $post) {
            $post->status = 2;
            $post->save();
            $notification = new Notification();
            $notification->title = "bài đăng {$post->title} hết hạn";
            $notification->content = "bài đăng {$post->title} hết hạn";
            $notification->type = 3;
            $notification->chanel = 2;
            $notification->user_id = $post->user_id;
            $notification->save();
            dispatch((new PushNotificationPost($notification))->onQueue('push_notification'));
        }
    }
}
