<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PostAnonymous
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $title
 * @property string|null $content
 * @property string|null $background
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PostAnonymous whereUsername($value)
 * @mixin \Eloquent
 */
class PostAnonymous extends Model
{
    protected $table = 'tbl_post_anonymous';
    protected $fillable = [
        'id',
        'username',
        'title',
        'content',
        'background'
    ];

    public function comments() {
        return $this->hasMany(Comment::class, 'post_id')->where('post_type', 3);
    }

    function getBackgroundAttribute($value) {
        return url('storage/upload/'.$value);
    }
}
