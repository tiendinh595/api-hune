<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $title
 * @property string|null $content
 * @property string|null $background
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment anonymous()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment comic()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment confidential()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUsername($value)
 * @mixin \Eloquent
 * @property int|null $post_id
 * @property int|null $post_type 1: confidential, 2 comic, 3: anonymous
 * @property int|null $status 1: active, 2: inactive
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment wherePostType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereStatus($value)
 */
class Comment extends Model
{
    protected $table = 'tbl_comment';
    protected $fillable = [
        'id',
        'username',
        'content',
        'post_id',
        'post_type',
        'status'
    ];

    public function scopeConfidential($query)
    {
        return $query->where('post_type', 1);
    }

    public function scopeComic($query)
    {
        return $query->where('post_type', 2);
    }

    public function scopeAnonymous($query)
    {
        return $query->where('post_type', 3);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
