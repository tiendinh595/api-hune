<?php
namespace App\Http\Utils;
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 7/21/2017
 * Time: 10:55 AM
 */
class Response
{
    public static function success( $data = [], $metadata = [], $msg = 'successfully')
    {
        return response()->json([
            'code' => 200,
            'msg' => $msg,
            'data' => $data,
            'meta_data' => $metadata
        ]);
    }

    public static function error($msg = 'error', $code = 500)
    {
        return response()->json([
            'code' => $code,
            'msg' => $msg
        ]);
    }

    public static function not_found($msg = 'not found')
    {
        return self::error($msg, 404);
    }

    public static function not_permission($msg = 'can not access')
    {
        return self::error($msg, 403);
    }

    public static function exists($msg = 'can not access')
    {
        return self::error($msg, 409);
    }
}