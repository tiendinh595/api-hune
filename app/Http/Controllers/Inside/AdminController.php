<?php

namespace App\Http\Controllers\Inside;

use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mockery\CountValidator\Exception;
use Socialite;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{

    function getProfile()
    {
        return view('admin.profile');
    }

    function postProfile(Request $request)
    {
//        Validator::make($request->all(), [
//            'name' => 'required',
//            'old_password' => 'required|old_password'
//        ], [
//            'required' => ':attribute bắt buộc phải nhập',
//            'old_password' => 'Mật khẩu cũ không đúng'
//        ], [
//            'name' => 'Tên',
//            'old_password' => 'Mật khẩu cũ'
//        ])->validate();


        $admin = Admin::find(auth()->guard('admin')->user()->id);
        $admin->name = $request->name;
        if ($request->new_password)
            $admin->password = Hash::make($request->new_password);

        if ($admin->save()) {
            $alert = [
                'class' => 'success',
                'msg' => 'Cập nhật thành công'
            ];
        } else {
            $alert = [
                'class' => 'danger',
                'msg' => 'Cập nhật thất bại'
            ];
        }

        $request->session()->flash('alert', $alert);
        return back();
    }

    function login(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            if (!auth()->guard('admin')->attempt(['username' => $request->username, 'password' => $request->password], true))
                $request->session()->flash('msg', 'Đăng nhập thất bại');
            else
                return redirect('/');
        }

        return view('admin.login');
    }

    /**
     * Redirect the Admin to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')
            ->with(['hd' => config('services.google.redirect')])->redirect();
    }

    /**
     * Obtain the Admin information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        try {
            $admin = Socialite::driver('google')->stateless()->user();

            $u = Admin::where('email', $admin->email)->first();
            if ($u) {
                $u->username = $admin->email;
                $u->name = $admin->name;
                $u->avatar = $admin->avatar;
                $u->password = Hash::make(1234);
                $u->save();
                Auth::guard('admin')->attempt(['username' => $admin->email, 'password' => '1234'], true);
                return redirect('/inside');
            } else {
                $request->session()->flash('msg', 'Đăng nhập thất bại');
                return redirect('/inside/login');

            }
        } catch (Exception $e) {
            echo 'Login fail';
        }
    }

    function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/inside/login');
    }

    function index(Request $request)
    {
        $users = Admin::all();
        return view('admin.list', ['users' => $users]);
    }

    function deleteUser(Request $request)
    {
        $email = $request->get('email');

        $user = Admin::where('username', $email)->first();

        if (!$user) {
            return ['status' => 'error', 'msg' => 'Không tồn tại quản trị viên này'];
        }

        if ($email == auth()->user()->username) {
            return ['status' => 'error', 'msg' => 'Không thể xóa'];
        }

        $user->delete();
        return ['status' => 'success', 'msg' => 'Xoá thành công'];
    }

    function getAddUser()
    {
        $permissions = Permission::all()->groupBy('group');

        return view('admin.form-add-admin', ['permissions' => $permissions]);
    }


    function postAddUser(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'permissions' => 'required'
        ]);

        try {
            DB::beginTransaction();
            $email = $request->get('email');
            $name = $request->get('name');
            $user = Admin::where('username', $email)->first();
            if ($user) {
                throw new \Exception('Quản tri viên đã tồn tại');
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('Không đúng định dạng email');
            }

            $user = new Admin();
            $user->name = $name;
            $user->username = $email;
            $user->email = $email;
            $user->password = Hash::make(1234);
            $user->save();

            foreach ($request->get('permissions') as $permission) {
                $user->givePermissionTo($permission);
            }
            DB::commit();

            return redirect()->back()->with('msg', 'Thêm thành công');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }

    function getEditUser($id)
    {
        $data = [
            'user'=>  Admin::findOrFail($id),
            'permissions'=>Permission::all()->groupBy('group'),
            'user_has_permissions' => []
        ];

        $user_has_permissions = $data['user']->permissions()->select('name')->get()->toArray();
        foreach ($user_has_permissions as $p) {
            $data['user_has_permissions'][] = $p['name'];
        }

        return view('admin.form-edit-admin', $data);
    }

    function  postEditUser(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'permissions' => 'required'
        ]);

        try {
            DB::beginTransaction();
            $email = $request->get('email');
            $name = $request->get('name');

            $user = Admin::findOrFail($id);
            $user->name = $name;
            $user->username = $email;
            $user->email = $email;
            $user->password = Hash::make(1234);
            $user->save();

            foreach (Permission::all() as $p) {
                try {
                    $user->revokePermissionTo($p->name);
                } catch (\Exception $e) {
                    continue;
                }
            }



            foreach ($request->get('permissions') as $permission) {
                try {
                    $user->givePermissionTo($permission);
                } catch (\Exception $e) {
                    continue;
                }
            }
            DB::commit();

            return redirect()->back()->with('msg', 'Thêm thành công');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }

    function getPermission()
    {
        $permissions = Permission::all();
        return view('admin.list-permissions', ['permissions' => $permissions]);
    }

    function getAddPermission()
    {
        return view('admin.form-add-permission');
    }

    function postAddPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'performs' => 'required',
        ]);
        try {

            DB::beginTransaction();
            foreach ($request->get('performs') as $perform) {

                Permission::create([
                    'name' => $perform . '-' . $request->get('name'),
                    'description' => $perform . ' ' . $request->get('name'),
                    'guard_name' => 'admin',
                    'group' => $request->get('name'),
                ]);
            }
            DB::commit();

            return redirect()->back()->with('msg', 'Thêm quyền thành công');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['messagge' => $e->getMessage()]);
        }

    }

    function getEditPermission($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.form-edit-permission', ['permission' => $permission]);
    }

    function postEditPermission(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        try {
            $permission = Permission::findOrFail($id);
            $permission->name = $request->get('name');
            $permission->description = $request->get('description');
            $permission->save();

            return redirect()->back()->with('msg', 'Cập nhật thành công');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['messagge' => $e->getMessage()]);
        }

    }
}
