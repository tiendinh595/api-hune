<?php

namespace App\Http\Controllers\Inside;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use YoHang88\LetterAvatar\LetterAvatar;

class UserController extends Controller
{
    function getIndex(Request $request)
    {
        $users = User::orderBy('id', 'DESC');

        if ($request->getQueryString()) {
            if ($request->input('UserName'))
                $users = $users->where('full_name', 'like', "%{$request->input('UserName')}%");
            $url_paginate = '/inside/user?' . preg_replace("/\??\&?page=([0-9]+)/", "", $request->getQueryString());
        } else {
            $url_paginate = '/inside/user';
        }

        $users = $users->paginate(20)->withPath($url_paginate);

        return view('user.list', ['users' => $users]);
    }

    function getEdit(Request $request, $id)
    {
        $data = [
            'user' => User::findOrFail($id),
        ];
        return view('user.form-edit', $data);
    }

    function postEdit(Request $request, $id)
    {
        $category = User::findOrFail($id);

        $data = $request->all();

        if ($data['password'] != '')
            $data['password'] = Hash::make($data['password']);
        else
            unset($data['password']);

        $category->update($data);
        return redirect()->back()->with('msg', 'Cập nhật thành công');
    }

    function getAdd()
    {
        return view('user.form-add');
    }

    function postAdd(Request $request)
    {
        // grab credentials from the request


        Validator::make($request->all(), [
            'full_name' => 'required',
            'phone' => 'required',
            'password' => 'required',
        ])->validate();

        $name_avatar = explode(' ', $request->get('full_name'));
        $letter_avatar = substr(trim(end($name_avatar)), 0, 1);

        $avatar = new LetterAvatar($letter_avatar, 'circle', 64);
        $file_name = time() . '.jpg';
        $sub_dir = date('Y/m/d');
        $upload_dir = storage_path('upload');
        $dir_save = $upload_dir . '/' . $sub_dir;
        if (!is_dir($dir_save))
            mkdir($dir_save, 0777, true);

        $avatar->saveAs($dir_save . '/' . $file_name);

        $user = new User();
        $user->full_name = $request->get('full_name');
        $user->phone = $request->get('phone');
        $user->password = Hash::make($request->get('password'));
        $user->active_code = rand(1000, 9999);
        $user->avatar = $sub_dir . '/' . $file_name;
        $user->save();

        return redirect()->back()->with('msg', 'Thêm user mới thành công');

    }


    function getDelete($id)
    {
        try {
            User::findOrFail($id)->delete();
            return 'ok';
        } catch (\Exception $e) {
            return 'fail';
        }
    }

    function getSearch(Request $request)
    {
        $publishers = User::where('full_name', 'like', "%{$request->get('query')}%")->orWhere('phone', 'like', "%{$request->get('query')}%")->get();
        $result = [
            'query' => $request->get('query'),
            'suggestions' => []
        ];
        foreach ($publishers as $u) {
            $result['suggestions'][] = ['value' => $u->full_name . '-' . $u->phone . ' - '. ($u->fcm_token!=''?'available':'unavailable'), 'data' => $u->id];
        }
        return $result;
    }

}
