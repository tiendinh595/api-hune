<?php

namespace App\Http\Controllers\Inside;

use Arcanedev\LogViewer\Facades\LogViewer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    function index() {
        return view('index.index');
    }
}
