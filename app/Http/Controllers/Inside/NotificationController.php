<?php

namespace App\Http\Controllers\Inside;

use App\Http\Controllers\Controller;
use App\Jobs\PushNotification;
use App\Notification;
use App\User;
use App\Utils\Firebase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    function getIndex(Request $request)
    {
        $notifications = Notification::orderBy('id', 'DESC');

        if ($request->getQueryString()) {
            if ($request->input('NotificationName'))
                $notifications = $notifications->where('NotificationName', 'like', "%{$request->input('NotificationName')}%");
            $url_paginate = '/inside/notification?' . preg_replace("/\??\&?page=([0-9]+)/", "", $request->getQueryString());
        } else {
            $url_paginate = '/inside/notification';
        }

        $notifications = $notifications->paginate(20)->withPath($url_paginate);

        return view('notification.list', ['notifications' => $notifications]);
    }

    function getPush() {
        return view('notification.form-add');
    }

    function postPush(Request $request) {
        Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'type' => 'required',
            'chanel' => 'required',
        ])->validate();

        $data = $request->except(['_token']);


        $notification = new Notification($data);
        $notification->save();

        $job = (new PushNotification($notification))->onQueue('push_notification');
        $this->dispatch($job);

        return redirect()->back()->with('msg', 'Puss notification success');
    }

    function getEdit(Request $request, $id)
    {
        $data = [
            'notification' => Notification::findOrFail($id),
        ];
        return view('notification.form-edit', $data);
    }

    function postEdit(Request $request, $id)
    {

        Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ])->validate();

        $notification = Notification::findOrFail($id);

        $data = $request->all();

        $notification->update($data);
        return redirect()->back()->with('msg', 'Cập nhật thành công');
    }

    function getDelete($id)
    {
        try {
            Notification::findOrFail($id)->delete();
            return 'ok';
        } catch (\Exception $e) {
            return 'fail';
        }
    }

    function getSearch(Request $request) {
        $publishers = Notification::where('full_name', 'like', "%{$request->get('query')}%")->get();
        $result = [
            'query' => $request->get('query'),
            'suggestions' => []
        ];
        foreach ($publishers as $u) {
            $result['suggestions'][] = ['value' => $u->full_name.'-'.$u->phone, 'data' => $u->id];
        }
        return $result;
    }

}
