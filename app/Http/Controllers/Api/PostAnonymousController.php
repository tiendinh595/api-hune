<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\PostAnonymous;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class PostAnonymousController extends ApiController
{
    /**
     * @SWG\Get(
     *   path="/post-anonymous",
     *   tags={"post-anonymous"},
     *   summary="get all post-anonymous",
     *    @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAllPostAnonymous(Request $request)
    {
        // grab credentials from the request
        try {
            $limit = (int)$request->get('limit', 15);

            $post_anonymous = PostAnonymous::orderBy('id', 'DESC')->paginate($limit);

            $metadata = [];
            $metadata['total'] = $post_anonymous->total();
            $metadata['current_page'] = $post_anonymous->currentPage();
            $metadata['has_more_pages'] = $post_anonymous->hasMorePages();

            if ($post_anonymous->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url') . 'post-anonymous?limit=' . $metadata['current_page'] . '&page=' . ($metadata['current_page'] + 1);

            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($post_anonymous->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Get(
     *   path="/post-anonymous/{id}",
     *   tags={"post-anonymous"},
     *   summary="get detail post anonymous",
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getDetail(Request $request, $id)
    {
        // grab credentials from the request
        try {
            return Response::success(PostAnonymous::findOrFail($id));
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *   path="/post-anonymous",
     *   tags={"post-anonymous"},
     *   summary="add new post",
     *   @SWG\Parameter(
     *     name="username",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="background",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postAddNewPost(Request $request)
    {
        // grab credentials from the request
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required|min:3|max:50',
                'title' => 'required|min:3|max:255',
                'content' => 'required',
                'background' => 'required',
            ]);

            if($validator->fails())
                return Response::error("Tham số không hợp lệ");

            $data = $request->only(['username', 'title', 'content', 'background']);
            PostAnonymous::create($data);
            return Response::success();
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

}
