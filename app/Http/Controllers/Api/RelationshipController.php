<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Comic;
use App\Relationship;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class RelationshipController extends ApiController
{

    /**
     * @SWG\Get(
     *   path="/relationship",
     *   tags={"relationship"},
     *   summary="get all relationship",
     *    @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="outgoing",
     *     in="query",
     *     description="value: 0, 1",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description="1: pending, 2: accepted",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAll(Request $request)
    {
        // grab credentials from the request
        try {
            $user = $this->authenticate();

            $validator = Validator::make($request->all(), [
                'outgoing' => Rule::in([0, 1]),
                'status' => Rule::in([1, 2]),
            ]);

            if ($validator->fails()) {
                return Response::error('Nhập thiếu thông tin');
            }

            $outgoing = $request->get('outgoing', 0);
            $status = $request->get('status', 1);
            $limit = $request->get('limit', 15);

            $column_where = $outgoing == 0 ? 'receiver_id' : 'sender_id';

            $relationships = Relationship::where($column_where, $user->id)->where('status', $status)->with(['sender', 'destination'])->paginate($limit);

            $metadata = [];
            $metadata['total'] = $relationships->total();
            $metadata['current_page'] = $relationships->currentPage();
            $metadata['has_more_pages'] = $relationships->hasMorePages();

            if ($relationships->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url') . 'relationship?limit=' . $limit . '&page=' . ($metadata['current_page'] + 1) . '&status=' . $status . '&outgoing=' . $outgoing;

            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($relationships->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Post(
     *   path="/relationship",
     *   tags={"relationship"},
     *   summary="send friend request",
     *    @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="receiver_id",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postSendRequest(Request $request)
    {
        // grab credentials from the request
        try {
            $validator = Validator::make($request->all(), [
                'receiver_id' => 'required',
            ]);

            if ($validator->fails()) {
                return Response::error('Nhập thiếu thông tin');
            }

            $receiver_id = $request->get('receiver_id');
            $user = $this->authenticate();

            $check_exists = Relationship::whereRaw(("(sender_id='{$user->id}' and receiver_id='{$receiver_id}') or (sender_id='{$receiver_id}' and receiver_id='{$user->id}')"))->first();

            if ($check_exists)
                return Response::error("Bạn và người này đã gửi yêu cầu kết bạn với nhau");

            $data = [
                'sender_id' => $user->id,
                'receiver_id' => $receiver_id
            ];

            Relationship::create($data);

            return Response::success([], [], "Gửi yêu cầu thành công");

        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Put(
     *   path="/relationship",
     *   tags={"relationship"},
     *   summary="update relationship",
     *    @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="relationship_id",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description=" 2: accepted, 3: reject",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function putUpdateStatusRelationship(Request $request)
    {
        // grab credentials from the request
        try {
            $user = $this->authenticate();

            $validator = Validator::make($request->all(), [
                'relationship_id' => 'required',
                'status' => Rule::in([2, 3])
            ]);

            if ($validator->fails()) {
                return Response::error('Tham số không hợp lệ');
            }

            $relationship_id = $request->get('receiver_id');
            $status = $request->get('status');

            $relationship = Relationship::where('receiver_id', $user->id)->where('id', $relationship_id)->firstOrFail();
            if ($status == 2) {
                $relationship->status = $status;
                return Response::success([], [], "Chấp nhận hẹn hò thành công");
            } else {
                $relationship->delete();
                return Response::success([], [], "Từ chối hẹn hò thành công");
            }

        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
