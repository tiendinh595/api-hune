<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Confidential;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ConfidentialController extends ApiController
{
    /**
     * @SWG\Get(
     *   path="/confidential",
     *   tags={"confidential"},
     *   summary="get all confidential",
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAllConfidential(Request $request)
    {
        // grab credentials from the request
        try {
            $limit = (int)$request->get('limit', 15);

            $confidentials = Confidential::orderBy('id', 'DESC')->paginate($limit);

            $metadata = [];
            $metadata['total'] = $confidentials->total();
            $metadata['current_page'] = $confidentials->currentPage();
            $metadata['has_more_pages'] = $confidentials->hasMorePages();

            if($confidentials->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url').'confidential?limit='.$metadata['current_page'].'&page='.($metadata['current_page']+1);

            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($confidentials->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Get(
     *   path="/confidential/{id}",
     *   tags={"confidential"},
     *   summary="get detail confidential",
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getDetail(Request $request, $id)
    {
        // grab credentials from the request
        try {
            return Response::success(Confidential::findOrFail($id));
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
