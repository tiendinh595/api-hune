<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Post;
use App\User;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CommentController extends ApiController
{
    /**
     * @SWG\Post(
     *   path="/comment",
     *   tags={"comment"},
     *   summary="comment",
     *   @SWG\Parameter(
     *     name="post_id",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="post_type",
     *     in="query",
     *     description="1: confidential, 2 comic, 3: anonymous",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="content",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postComment(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_type' => 'required',
                'post_id' => 'required',
                'user_name' => 'required',
                'content' => 'required'
            ]);

            if ($validator->fails())
                return Response::error("miss parameter");

            $data = $request->only(['post_type', 'post_id', 'user_name', 'content']);
            Comment::create($data);

            return Response::success();
        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Get(
     *   path="/comment",
     *   tags={"comment"},
     *   summary="get all comment",
     *   @SWG\Parameter(
     *     name="post_type",
     *     in="query",
     *     description="1: confidential, 2 comic, 3: anonymous",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAllComment(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'post_type' => 'required'
            ]);

            if ($validator->fails())
                return Response::error("miss parameter");

            $limit = $request->get('limit', 15);
            $post_type = $request->get('post_type');
            $comments = Comment::where('post_type', $post_type)->active()->orderBy('id', 'DESC')->paginate($limit);
            $metadata = [];
            $metadata['total'] = $comments->total();
            $metadata['current_page'] = $comments->currentPage();
            $metadata['has_more_pages'] = $comments->hasMorePages();

            if ($comments->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url') . 'comment?limit=' . $metadata['current_page'] . '&page=' . ($metadata['current_page'] + 1) . '&post_type=' . $post_type;
            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($comments->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error();
        }
    }
}
