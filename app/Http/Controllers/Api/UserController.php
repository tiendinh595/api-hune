<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegister;
use App\Favourite;
use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Relationship;
use App\User;
use App\Utils\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use YoHang88\LetterAvatar\LetterAvatar;

class UserController extends ApiController
{

    /**
     * @SWG\Post(
     *   path="/user/login-facebook",
     *   tags={"user"},
     *   summary="login using facebook",
     *   @SWG\Parameter(
     *     name="fcm_token",
     *     in="formData",
     *     description="firebase cloud message access token",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="facebook_token",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postLoginFacebook(Request $request)
    {
        // grab credentials from the request
        try {
            $url_api_verify = 'https://graph.facebook.com/app?access_token=' . $request->get('facebook_token');
            $app_of_token = json_decode(Http::get($url_api_verify), true);
            if (isset($app_of_token['error']))
                return Response::error('Invalid access token');

            if ($app_of_token['id'] != config('app.app_facebook_id'))
                return Response::error('Invalid access token');

            $url_api_profile = 'https://graph.facebook.com/me?fields=id,name&access_token=' . $request->get('facebook_token');
            $profile = json_decode(Http::get($url_api_profile), true);

            if (isset($profile['error']))
                return Response::error('Invalid access token');

            $fb_id = $profile['id'];

            $link_graph_avatar = "http://graph.facebook.com/{$profile['id']}/picture?type=square&redirect=false&height=256&width=256";
            $graph_object = json_decode(Http::get($link_graph_avatar), true);
            $avatar = $graph_object['data']['url'];


            $user = User::where('facebook_id', $fb_id)->first();

            if (!$user) {
                $user = new User();
                $user->password = Hash::make(1234);
                $user->facebook_id = $fb_id;
                $user->full_name = $profile['name'];
                $user->avatar = $avatar;
                $user->save();
            }

            $credentials = [
                'facebook_id' => $fb_id,
                'password' => 1234
            ];

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return Response::not_found('login fail');
                }
            } catch (JWTException $e) {
                return Response::error();
            }

            $fcm_token = $request->get('fcm_token', null);

            $user->token = $token;
            if ($fcm_token != null)
                $user->fcm_token = $fcm_token;
            $user->save();
            return Response::success($user);
        } catch (Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Get(
     *   path="/user/profile",
     *   tags={"user"},
     *   summary="profile",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="query",
     *     description="default to get profile of current user, else get profile by user id",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getProfile(Request $request)
    {
        // grab credentials from the request
        try {
            $user = $this->authenticate();

            $user_id = $request->get('user_id', null);

            if ($user_id != null) {
                $relationship = Relationship::whereRaw("(sender_id='{$user->id}' and receiver_id='{$user_id}') or (sender_id='{$user_id}' and receiver_id='{$user->id}')")->first(['status']);
                $user = User::findOrFail($user_id);
                unset($user->token);

                $user->relationship = $relationship ? $relationship : 'none';
            }

            return Response::success($user);
        } catch (Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Put(
     *   path="/user/profile",
     *   tags={"user"},
     *   summary="update profile",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="fcm_token",
     *     in="formData",
     *     description="firebase token",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="full_name",
     *     in="formData",
     *     description="full name",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="sex",
     *     in="formData",
     *     description="1: female, 2:male",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="birthday",
     *     in="formData",
     *     description="format: yyy-mm-dd",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="avatar",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="marital_status",
     *     in="formData",
     *     description="1: signle, 2: married, 3: divorced, 4: widowed ",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="job",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="destination",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="latitude",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="longitude",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function putUpdate(Request $request)
    {
        // grab credentials from the request
        try {
            $user = $this->authenticate();

            $validator = Validator::make($request->all(), [
                'full_name' => 'min:3|max:100',
                'sex' => [
                    Rule::in([1, 2])
                ],
                'marital_status' => [
                    Rule::in([1, 2, 3, 4])
                ],
                'birthday' => 'date',
            ]);

            if ($validator->fails()) {
                return Response::error('Nhập thiếu thông tin');
            }

            $user = User::findOrFail($user->id);
            $user->full_name = $request->get('full_name', $user->full_name);
            $user->sex = $request->get('sex', $user->sex);
            $user->birthday = $request->get('birthday', $user->birthday);
            $user->fcm_token = $request->get('fcm_token', $user->fcm_token);
            $user->marital_status = $request->get('marital_status', $user->marital_status);
            $user->job = $request->get('job', $user->job);
            $user->latitude = $request->get('latitude', $user->latitude);
            $user->longitude = $request->get('longitude', $user->longitude);
            $user->destination = $request->get('destination', $user->destination);

            if ($request->get('avatar'))
                $user->avatar = $request->get('avatar');

            if ($user->phone == '')
                $user->phone = $request->get('phone', $user->phone);
            $user->save();

            return Response::success($user);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }


    /**
     * @SWG\Post(
     *   path="/user/verify",
     *   tags={"user"},
     *   summary="verify",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="ack_token",
     *     in="formData",
     *     description="Account kit access token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postVerify(Request $request)
    {
        // grab credentials from the request
        try {
            $user = $this->authenticate();
            $user = User::findOrFail($user->id);

            if ($user->status == 1) {
                $ack_token = $request->ack_token;

                $access_token = 'AA|' . config('app.account_kit_id') . '|' . config('app.account_kit_secret');


                $url = 'https://graph.accountkit.com/v1.0/access_token?grant_type=authorization_code&code=' . $ack_token . '&access_token=' . $access_token;

                $result = json_decode(Http::get($url), true);

                if (isset($result['error']))
                    return Response::error($result['error']['message']);


                $url = 'https://graph.accountkit.com/v1.0/me?access_token=' . $result['access_token'];
                $result = json_decode(Http::get($url), true);


                if (isset($result['error']))
                    return Response::error($result['error']['message']);

                if (User::where('phone', $result['phone']['number'])->orWhere('phone', str_replace('+84', 0, $result['phone']['number']))->first())
                    return Response::error('Số điện thoại này đã tồn tại');

                if ($user->facebook_id == '') {
                    if (strpos($user->phone, 0) >= 0)
                        $phone_stock = substr($user->phone, 1);
                    $phone_stock = '+84' . str_replace('+84', '', $phone_stock);
                    if ($result['phone']['number'] != $phone_stock)
                        return Response::error('Số điện thoại không khớp với token');
                } else {
                    $user->phone = $result['phone']['number'];
                }

                $user->status = 2;
                $user->save();

                return Response::success($user);
            } else {
                return Response::error('Không thể xác thực tài khoản này (code: 01)');
            }

        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *   path="/user/nearby-place",
     *   tags={"user"},
     *   summary="find users nearby place",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="latitude",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="longitude",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getNearbyPlace(Request $request)
    {
        // grab credentials from the request
        try {
            $validator = Validator::make($request->all(), [
                'latitude'=>'required',
                'longitude'=>'required',
            ]);

            if ($validator->fails()) {
                return Response::error('Nhập thiếu thông tin');
            }

            $user = $this->authenticate();
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');
            $limit = $request->get('limit', 15);

            $users = User::orderBy('distance')
                ->where('id', '<>', $user->id)
                ->select(DB::raw("id, avatar, full_name, longitude, latitude, (6371 * acos(cos(radians('{$latitude}')) * cos(radians(latitude)) * cos(radians(longitude) - radians('{$longitude}')) + sin(radians('{$latitude}')) * sin(radians(latitude)))) AS distance"))->paginate($limit);

            $metadata = [];
            $metadata['total'] = $users->total();
            $metadata['current_page'] = $users->currentPage();
            $metadata['has_more_pages'] = $users->hasMorePages();

            if($users->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url').'user/nearby-place?limit='.$limit.'&page='.($metadata['current_page']+1).'&latitude='.$latitude.'&longitude='.$longitude;

            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($users->getCollection(), $metadata);
        } catch (Exception $e) {
            return Response::error();
        }
    }


    /**
     * @SWG\Get(
     *   path="/user/validate-token",
     *   tags={"user"},
     *   summary="validate token",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function validateToken() {
        try {
            $user = $this->authenticate();
            return Response::success();
        } catch (\Exception $e) {
            return Response::error();
        }
    }

}
