<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Events\UserRegister;
use App\Favourite;
use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Relationship;
use App\User;
use App\Utils\Http;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use YoHang88\LetterAvatar\LetterAvatar;

class ChatController extends ApiController
{

    /**
     * @SWG\Get(
     *   path="/chat/{friend_id}",
     *   tags={"chat"},
     *   summary="get list chat",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="friend_id",
     *     in="path",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getDetail(Request $request, $friend_id)
    {
        // grab credentials from the request
        try {

//            $user = $this->authenticate();

            $limit = $request->get('limit', 15);

            $chats = Chat::whereRaw("(sender_id = 1 and receiver_id = 2) or (receiver_id = 1 and sender_id = 2)")
                        ->join('tbl_user', function ($join) use ($friend_id) {
                            $join->where('tbl_user.id', $friend_id);
                        })
                        ->select(DB::raw("tbl_chat.*"))
                        ->with(['sender', 'receiver'])
                        ->paginate($limit);


            $metadata['total'] = $chats->total();

            $metadata['current_page'] = $chats->currentPage();
            $metadata['has_more_pages'] = $chats->hasMorePages();

            if ($chats->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url') . 'chat/'.$friend_id.'?limit=' . $metadata['current_page'] . '&page=' . ($metadata['current_page'] + 1);
            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($chats->getCollection(), $metadata);
        } catch (Exception $e) {
            return Response::error($e->getMessage());
        }
    }


    /**
     * @SWG\Post(
     *   path="/chat",
     *   tags={"chat"},
     *   summary="send message",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="sender_id",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="receiver_id",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="message",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="media",
     *     in="formData",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="media_type",
     *     in="formData",
     *     description="1: image, 2: video",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postChat(Request $request)
    {
        // grab credentials from the request
        try {

            $user = $this->authenticate();

            $validator = Validator::make($request->all(), [
                'sender_id' => [
                    'required',
                    Rule::in($user->id)
                ],
                'receiver_id' => 'required',
                'message' => 'required|min:1|max:255',
                'media_type' => Rule::in([1, 2])
            ]);

            if ($validator->fails()) {
                return Response::error('Tham số không hợp lệ');
            }

            User::findOrFail($request->get('receiver_id'));

            $data = $request->only(['sender_id', 'receiver_id', 'message', 'media_type']);
            Chat::create($data);

            return Response::success();
        } catch (Exception $e) {

        }
    }

    /**
     * @SWG\Get(
     *   path="/chat",
     *   tags={"chat"},
     *   summary="get list chat",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAll(Request $request)
    {
        // grab credentials from the request
        try {

            $user = $this->authenticate();

            $page = Paginator::resolveCurrentPage();
            $limit = $request->get('limit', 15);

            $chats = DB::select(DB::raw("
                SELECT t.message , (t.sender_id+t.receiver_id) as dist, u.full_name,
                CASE
                    WHEN sender_id = '{$user->id}' then receiver_id
                    ELSE sender_id
                END friend_id, t.created_at
                FROM (
                    select * from tbl_chat where sender_id = '{$user->id}' or receiver_id = '{$user->id}' order by created_at DESC
                ) t, tbl_user u
                where u.id =  CASE
                    WHEN sender_id = '{$user->id}' then receiver_id
                    ELSE sender_id
                END
                group by dist
            "));

            $metadata['total'] = count($chats);
            $chats = new Paginator($chats, $limit, $page);

            $metadata['current_page'] = $chats->currentPage();
            $metadata['has_more_pages'] = $chats->hasMorePages();

            if ($chats->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url') . 'chat?limit=' . $metadata['current_page'] . '&page=' . ($metadata['current_page'] + 1);
            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($chats->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

}
