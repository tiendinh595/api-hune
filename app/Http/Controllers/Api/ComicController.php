<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Comic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ComicController extends ApiController
{
    /**
     * @SWG\Get(
     *   path="/comic",
     *   tags={"comic"},
     *   summary="get all comic",
     *    @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="",
     *     required=false,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="limit of record to return, default 15",
     *     required=false,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getAllComic(Request $request)
    {
        // grab credentials from the request
        try {
            $limit = (int)$request->get('limit', 15);

            $comics = Comic::orderBy('id', 'DESC')->paginate($limit);

            $metadata = [];
            $metadata['total'] = $comics->total();
            $metadata['current_page'] = $comics->currentPage();
            $metadata['has_more_pages'] = $comics->hasMorePages();

            if($comics->hasMorePages()) {
                $metadata['next_link'] = config('app.api_url').'comic?limit='.$metadata['current_page'].'&page='.($metadata['current_page']+1);

            } else {
                $metadata['next_link'] = null;
            }

            return Response::success($comics->getCollection(), $metadata);
        } catch (\Exception $e) {
            return Response::error();
        }
    }

    /**
     * @SWG\Get(
     *   path="/comic/{id}",
     *   tags={"comic"},
     *   summary="get detail comic",
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function getDetail(Request $request, $id)
    {
        // grab credentials from the request
        try {
            return Response::success(Comic::findOrFail($id));
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
