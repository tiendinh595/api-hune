<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Utils\Response;
use App\Post;
use App\User;
use App\Utils\File;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class FileController extends ApiController
{
    /**
     * @SWG\Post(
     *   path="/file/upload",
     *   tags={"file"},
     *   summary="upload file",
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="file",
     *     in="formData",
     *     description="",
     *     required=true,
     *     type="file",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="successfully"
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="not found"
     *   ),
     *   @SWG\Response(
     *     response="403",
     *     description="access denied"
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    function postUpload(Request $request)
    {
        try {
            $user = $this->authenticate();
            $this->validate($request, [
               'file'=>'image|required'
            ]);

            $name = time() . '.' . $request->file->extension();
            $url = File::uploadFile($_FILES['file'], $name);

            return Response::success([
                'url'=>$url
            ]);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }


}
