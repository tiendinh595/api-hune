<?php
/**
 * Created by PhpStorm.
 * User: Vũ Tiến Định
 * Date: 7/21/2017
 * Time: 10:11 AM
 */

namespace App\Http\Controllers;
use App\Http\Utils\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host="tamsu.dev",
 *   basePath="/api/v1/",
 *   produces={"text/plain"},
 * 	 consumes={"application/json"},
 *   @SWG\Info(
 *     version="1.0.0",
 *     title="documents api",
 *     description="https://tamsu.dev/api/v1",
 *     termsOfService=""
 *   ),
 * )
 */
class ApiController extends Controller
{

    function authenticate()  {
        $user = JWTAuth::parseToken()->authenticate();
        if($user)
        {
            if($user->status == 3)
            {
                echo Response::not_permission("account is banned")->content();
                exit();
            }
        }

        return $user;
    }

}