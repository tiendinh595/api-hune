<?php
/**
 * Created by PhpStorm.
 * User: dinh
 * Date: 8/2/17
 * Time: 10:21 AM
 */

namespace App\Utils;


class Http
{
    static function get($url) {
        $handle=curl_init($url);
        curl_setopt($handle, CURLOPT_VERBOSE, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($handle);
        return $content;
    }
}