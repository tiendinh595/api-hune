<?php
namespace App\Utils;
/**
 * Created by PhpStorm.
 * User: TienDinh
 * Date: 6/7/2017
 * Time: 9:51 AM
 */
class File
{
    static function retrieve_remote_file_size($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);

        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);
        return $size;
    }

    static function save_remote_file($url, $name, $type = 'image')
    {
        $sub_dir = date('Y/m/d');
        $root = $type == 'image' ? \Config::get('define.upload_image_path') : \Config::get('define.upload_file_path');
        echo $dir_save = $root . '/' . $sub_dir;
        $destination_path = $dir_save . '/' . $name;
        if (!is_dir($dir_save))
            mkdir($dir_save, 0777, true);
        set_time_limit(0);
        $fp = fopen($destination_path, 'w+');
        $ch = curl_init(str_replace(" ", "%20", $url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return [
            $sub_dir . '/' . $name,
            $destination_path
        ];
    }

    static function uploadFile($file, $name) {
        $sub_dir = date('Y/m/d');
        $upload_dir =  storage_path('upload');
        $dir_save = $upload_dir . '/' . $sub_dir;
        if (!is_dir($dir_save))
            mkdir($dir_save, 0777, true);
        $destination_path = $dir_save . '/' . $name;
        copy($file['tmp_name'], $destination_path);
        return $sub_dir.'/'.$name;
    }
}