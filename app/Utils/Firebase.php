<?php
/**
 * Created by PhpStorm.
 * User: dinh
 * Date: 8/1/17
 * Time: 10:46 AM
 */

namespace App\Utils;


use App\Notification;
use Illuminate\Support\Facades\Log;

class Firebase
{

    public function send($registration_ids, Notification $notification) {
        $fields = array(
            'priority'=>'high',
            'registration_ids' => $registration_ids,
            'data' => [
                'title'=> $notification->title,
                'body'=> $notification->content,
                'post_id'=>$notification->post_id,
                'owner_post'=>$notification->owner_post,
                'type'=>$notification->type
            ],
            "notification"=> [
                'title'=> $notification->title,
                'text'=> $notification->content,
            ]
        );
        return $this->sendPushNotification($fields);
    }

    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent
    */
    private function sendPushNotification($fields) {

        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';

        //building headers for the request
        $headers = array(
            'Authorization: key=' . config('app.firebase_api_key'),
            'Content-Type: application/json'
        );

        //Initializing curl to open a connection
        $ch = curl_init();

        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);

        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);

        //adding headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //adding the fields in json format
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //finally executing the curl request
        $result = curl_exec($ch);
        if ($result === FALSE) {
            return  curl_error($ch);
        }

        //Now close the connection
        curl_close($ch);

        //and return the result
        return $result;
    }

}