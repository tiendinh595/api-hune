<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Relationship
 *
 * @property int $id
 * @property int|null $sender_id
 * @property int|null $receiver_id
 * @property int|null $status 1: pendding, 2: accepted, 3: reject
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereDestinationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Relationship whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Relationship extends Model
{
    protected $table = 'tbl_relationship';
    protected $fillable = [
        'id',
        'sender_id',
        'receiver_id',
        'status',
    ];

    public function sender() {
        return $this->hasMany(User::class, 'sender_id');
    }

    public function destination() {
        return $this->hasMany(User::class, 'receiver_id');
    }

}
