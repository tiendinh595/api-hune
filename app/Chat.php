<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'tbl_chat';

    protected $fillable = [
        'id',
        'sender_id',
        'receiver_id',
        'message',
        'media',
        'media_type'
    ];

    function getMediaAttribute($value) {
        return $value ?  url('storage/upload/'.$value) : '';
    }

    public function sender() {
        return $this->belongsTo(User::class, 'sender_id')->select(['id', 'avatar', 'full_name']);
    }

    public function receiver() {
        return $this->belongsTo(User::class, 'receiver_id')->select(['id', 'avatar', 'full_name']);
    }
}
