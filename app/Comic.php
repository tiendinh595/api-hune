<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comic
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $background
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comic whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Comic extends Model
{
    protected $table = 'tbl_comic';
    protected $fillable = [
        'id',
        'title',
        'content',
        'background'
    ];

    public function comments() {
        return $this->hasMany(Comment::class, 'post_id')->where('post_type', 2);
    }

    function getBackgroundAttribute($value) {
        return url('storage/upload/'.$value);
    }
}
