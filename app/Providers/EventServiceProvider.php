<?php

namespace App\Providers;

use App\Events\UserRegister;
use App\Listeners\SendCodeActive;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\UserRegister' => [
            'App\Listeners\SendCodeActive',
        ],
        'App\Events\AddNewPost' => [
            'App\Listeners\PushNotificationPostRelate',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
