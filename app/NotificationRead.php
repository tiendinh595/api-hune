<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\NotificationRead
 *
 * @property int $id
 * @property int|null $notification_id
 * @property int|null $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationRead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationRead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationRead whereNotificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationRead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationRead whereUserId($value)
 * @mixin \Eloquent
 */
class NotificationRead extends Model
{
    protected $table = 'tbl_notification_read';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'notification_id', 'user_id', 'created_at', 'updated_at'
    ];
}
