<?php

namespace App\Jobs;

use App\Notification;
use App\User;
use App\Utils\Firebase;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PushNotificationPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $notification = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            echo '-start--';
            $user = $this->notification->user;
            $registration_ids = [$user->fcm_token];
            $firebase = new Firebase();
            $result = $firebase->send($registration_ids, $this->notification);
            print_r($result);
            echo '--end--';
        } catch (\Exception $e) {
            \Log::error('post expired: '.$e->getMessage());
        }
    }
}
