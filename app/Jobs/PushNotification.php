<?php

namespace App\Jobs;

use App\Notification;
use App\User;
use App\Utils\Firebase;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $notification = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo '----start---';
        $registration_ids = [];

        if ($this->notification->chanel == 2)
            $registration_ids[] = $this->notification->user->fcm_token;
        else {
            $all_user = User::select('fcm_token')->get();
            foreach ($all_user as $u) {
                if ($u->fcm_token != '')
                    $registration_ids[] = $u->fcm_token;
            }
        }

        $firebase = new Firebase();
        $result = $firebase->send($registration_ids, $this->notification);
        echo '----end----';
    }
}
