<?php

namespace App\Listeners;

use App\Events\UserRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;

class SendCodeActive implements ShouldQueue
{
    use InteractsWithQueue;
    public $connection = 'database';
    public $queue = 'send-active-code';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(UserRegister $event)
    {
        $user = $event->user;
        if($user->phone) {
            Log::info("send code to phone {$user->active_code}");
        }
    }

    public function failed(UserRegister $event, $exception)
    {
        Log::error("error send code to phone: {$event->user->id}:  $exception->getMessage()");
    }
}
