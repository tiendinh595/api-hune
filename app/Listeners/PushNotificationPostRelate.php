<?php

namespace App\Listeners;

use App\Events\AddNewPost;
use App\Notification;
use App\Post;
use App\User;
use App\Utils\Firebase;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class PushNotificationPostRelate implements ShouldQueue
{
    use InteractsWithQueue;
    public $connection = 'database';
    public $queue = 'push_notification';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddNewPost $event
     * @return void
     */
    public function handle(AddNewPost $event)
    {
        try {
            $post = $event->post;

            if ($post->type == 1) {
                $type_condition = 2;
                $title = 'Việc làm phù hợp';
                $content = $post->title;
            } else {
                $type_condition = 1;
                $title = 'Ứng viên phù hợp';
                $content = $post->title;
            }


            $user = User::findOrFail($post->user_id);

            $radius = 6;


            $posts = Post::having('distance', '<', $radius)
                ->whereRaw("category_id in (select distinct category_id from tbl_post where user_id != {$user->id} and status = 1 and type={$type_condition})")
                ->where('type', $type_condition)->where('status', 1);

            $posts = $posts->with('user')
                ->select(DB::raw("user_id, type, (6371 * acos(cos(radians('{$post->latitude}')) * cos(radians(latitude)) * cos(radians(longitude) - radians('{$post->longitude}')) + sin(radians('{$post->latitude}')) * sin(radians(latitude)))) AS distance"))->get();

            if ($posts) {

                foreach ($posts as $p) {
                    $u = User::find($p->user_id);
                    $fcm_token = $u->fcm_token;
                    $registration_ids = [$fcm_token];

                    $notification = new Notification();
                    $notification->title = $title;
                    $notification->content = $content;
                    $notification->type = $post->type;
                    $notification->chanel = 2;
                    $notification->user_id = $p->user_id;
                    $notification->post_id = $post->id;
                    $notification->owner_post = $post->user_id;
                    $notification->save();

                    $firebase = new Firebase();
                    $firebase->send($registration_ids, $notification);

                }
            }

        } catch (\Exception $e) {
            \Log::error('post relate: '.$e->getMessage());
        }

    }
}
