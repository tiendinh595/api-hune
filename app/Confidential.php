<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Confidential
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $background
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Confidential whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Confidential extends Model
{
    protected $table = 'tbl_confidential';
    protected $fillable = [
        'id',
        'title',
        'content',
        'background'
    ];

    public function comments() {
        return $this->hasMany(Comment::class, 'post_id')->where('post_type', 1);
    }

    function getBackgroundAttribute($value) {
        return url('storage/upload/'.$value);
    }
}
