<?php

namespace Spatie\Permission\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {

        if (Auth::guard('admin')->guest()) {
            abort(403);
        }


        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if (Auth::guard('admin')->user()->can($permission)) {
                return $next($request);
            }
        }

        abort(403);
    }
}
