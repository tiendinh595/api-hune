<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['prefix'=>'inside'], function () {

    Route::any('/login', 'Inside\AdminController@login')->name('login');
    Route::any('/login/callback', 'Inside\AdminController@handleProviderCallback')->name('login-gg');
    Route::any('/login-gg', 'Inside\AdminController@redirectToProvider')->name('login-gg');

    Route::group(['middleware'=>'admin'], function() {
        Route::get('/', 'Inside\DashboardController@index');
        Route::get('profile', 'Inside\AdminController@getProfile');
        Route::post('profile', 'Inside\AdminController@postProfile');
        Route::get('logout', 'Inside\AdminController@logout')->name('logout');

        Route::group(['prefix'=>'administrator'], function () {
            Route::get('/', 'Inside\AdminController@index')->middleware('permission:list-admin');
            Route::get('/delete', 'Inside\AdminController@deleteUser')->name('delete-admin')->middleware('permission:delete-user');
            Route::get('/add', 'Inside\AdminController@getAddUser')->name('add-admin')->middleware('permission:add-user');
            Route::post('/add', 'Inside\AdminController@postAddUser')->name('add-admin')->middleware('permission:add-user');
            Route::get('/{id}/edit', 'Inside\AdminController@getEditUser')->name('edit-admin')->middleware('permission:edit-admin');
            Route::post('/{id}/edit', 'Inside\AdminController@postEditUser')->name('edit-admin')->middleware('permission:edit-admin');
            Route::get('/permission', 'Inside\AdminController@getPermission')->middleware('permission:list-permission');
            Route::get('/permission/add', 'Inside\AdminController@getAddPermission')->name('add-permission')->middleware('permission:add-permission');
            Route::post('/permission/add', 'Inside\AdminController@postAddPermission')->name('add-permission')->middleware('permission:add-permission');
            Route::get('/permission/{id}/edit', 'Inside\AdminController@getEditPermission')->name('edit-permission')->middleware('permission:edit-permission');
            Route::post('/permission/{id}/edit', 'Inside\AdminController@postEditPermission')->name('edit-permission')->middleware('permission:edit-permission');
        });

        Route::group(['prefix'=>'user'], function () {
            Route::get('/', 'Inside\UserController@getIndex')->middleware('permission:list-user');
            Route::get('/search', 'Inside\UserController@getSearch');
            Route::get('{id}/edit', 'Inside\UserController@getEdit')->name('member-edit')->middleware('permission:edit-user');
            Route::post('{id}/edit', 'Inside\UserController@postEdit')->middleware('permission:edit-user');
            Route::get('/add', 'Inside\UserController@getAdd')->name('member-add')->middleware('permission:add-user');
            Route::post('/add', 'Inside\UserController@postAdd')->middleware('permission:add-user');
            Route::get('{id}/delete', 'Inside\UserController@getDelete')->name('member-delete')->middleware('permission:delete-user');
        });

        Route::group(['prefix'=>'notification'], function () {
            Route::get('/', 'Inside\NotificationController@getIndex')->middleware('permission:list-notification');
            Route::get('/push', 'Inside\NotificationController@getPush')->middleware('permission:add-notification');
            Route::post('/push', 'Inside\NotificationController@postPush')->middleware('permission:add-notification');
            Route::get('{id}/edit', 'Inside\NotificationController@getEdit')->name('notification-edit')->middleware('permission:edit-notification');
            Route::post('{id}/edit', 'Inside\NotificationController@postEdit')->middleware('permission:edti-notification');
            Route::get('{id}/delete', 'Inside\NotificationController@getDelete')->name('notification-delete')->middleware('permission:delete-notification');
        });

    });
});