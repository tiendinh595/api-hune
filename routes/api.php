<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'], function () {
    Route::group(['prefix'=>'user'], function () {
        Route::post('/login-account', 'Api\UserController@postLoginAccount');
        Route::post('/login-facebook', 'Api\UserController@postLoginFacebook');
        Route::post('/change-password', 'Api\UserController@postChangePassword');
        Route::post('/forget-password', 'Api\UserController@postForgetPassword');
        Route::post('/register', 'Api\UserController@postRegister');
        Route::get('/profile', 'Api\UserController@getProfile');
        Route::put('/profile', 'Api\UserController@putUpdate');
        Route::post('/verify', 'Api\UserController@postVerify');
        Route::post('/nearby-place', 'Api\UserController@getNearbyPlace');
        Route::get('/validate-token', 'Api\UserController@validateToken');
    });

    Route::group(['prefix'=>'confidential'], function () {
        Route::get('/', 'Api\ConfidentialController@getAllConfidential');
        Route::get('/{id}', 'Api\ConfidentialController@getDetail');
    });

    Route::group(['prefix'=>'comic'], function () {
        Route::get('/', 'Api\ComicController@getAllComic');
        Route::get('/{id}', 'Api\ComicController@getDetail');
    });

    Route::group(['prefix'=>'post-anonymous'], function () {
        Route::get('/', 'Api\PostAnonymousController@getAllPostAnonymous');
        Route::post('/', 'Api\PostAnonymousController@postAddNewPost');
        Route::get('/{id}', 'Api\PostAnonymousController@getDetail');
    });

    Route::group(['prefix'=>'file'], function () {
        Route::post('/upload', 'Api\FileController@postUpload');
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('/', 'Api\CommentController@getAllComment');
        Route::post('/', 'Api\CommentController@postComment');
    });

    Route::group(['prefix'=>'relationship'], function () {
       Route::get('/', 'Api\RelationshipController@getAll');
       Route::post('/', 'Api\RelationshipController@postSendRequest');
       Route::put('/', 'Api\RelationshipController@putUpdateStatusRelationship');
    });

    Route::group(['prefix'=>'chat'], function () {
        Route::get('/', 'Api\ChatController@getAll');
        Route::post('/', 'Api\ChatController@postChat');
        Route::get('/{friend_id}', 'Api\ChatController@getDetail');
    });
});
